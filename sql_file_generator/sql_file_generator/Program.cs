﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace sql_file_generator
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> data = new List<string>();
            List<string> data2 = new List<string>();            
            /*StreamReader r = new StreamReader(@"skladinput.txt");
            while (!r.EndOfStream)
            {
                data.Add(r.ReadLine());
            }
            r.Close();*/
            Random rnd = new Random();

            StreamWriter w = new StreamWriter(@"outputpokupatel.txt", false, Encoding.Default, 10);
            List<int> dostavkaNotUsed = new List<int>();
            for (int i = 1; i < 101; i++)
                dostavkaNotUsed.Add(i);
            for (int i = 0; i < 100; i++)
            {
                DateTime date = DateTime.Now.AddDays(rand.Next(-200, 400));
                int to_use = rnd.Next(dostavkaNotUsed.Count);
                w.WriteLine($"({i + 1}, {rnd.Next(101)}, {rnd.Next(101)}, {rnd.Next(101)}, {dostavkaNotUsed[to_use]}, {rnd.Next(10,5000) * 100}, {rnd.Next(1,100)}, '{DateTime.Now.AddDays(rnd.Next(-500, 0)).ToShortDateString().Replace('.','/')}'),");
                dostavkaNotUsed.RemoveAt(to_use);
            }
            w.Close();
        }
        static Random rand = new Random();
        static string GetRandomTelNo()
        {
            StringBuilder telNo = new StringBuilder(12);
            int number;
            for (int i = 0; i < 3; i++)
            {
                number = rand.Next(0, 8); // digit between 0 (incl) and 8 (excl)
                telNo = telNo.Append(number.ToString());
            }
            telNo = telNo.Append("-");
            number = rand.Next(0, 743); // number between 0 (incl) and 743 (excl)
            telNo = telNo.Append(String.Format("{0:D3}", number));
            telNo = telNo.Append("-");
            number = rand.Next(0, 10000); // number between 0 (incl) and 10000 (excl)
            telNo = telNo.Append(String.Format("{0:D4}", number));
            return telNo.ToString();
        }
        static string getRandomINN()
        {
            string str = "";
            int len = (rand.Next(2) == 1) ? 10 : 12;
            for (int i = 0; i < len; i++)
                str += rand.Next(10).ToString();
            return str;
        }
    }
}
