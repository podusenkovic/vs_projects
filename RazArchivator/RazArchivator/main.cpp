#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <iterator>
#include <algorithm>
#include <bitset>
#include <time.h>
#include <string>

using namespace std;


bool myMapCmp(const pair<string, char>& first, const pair<string, char>& second) {
	return first.first.size() < second.first.size();
}

int main() {
	ifstream archive;
	archive.open("C:\\Users\\podus\\source\\repos\\WinRarOnMinimalkah\\WinRarOnMinimalkah\\output.sosi_jepy");
	int dictLen;
	dictLen = archive.get();

	map<string, char> dictionary;

	for (int i = 0; i < dictLen; i++) {
		char chr, code, zeros;
		chr = archive.get();
		code = archive.get(); 
		zeros = archive.get();
		for (int j = 0; j < unsigned int(zeros); j++)
			code >>= 1;
		string rdyCode = "";
		for (int j = int(zeros); j < 8; j++) {
			rdyCode.push_back(((code | 1u) == code) ? '1' : '0');
			code >>= 1;
		}
		reverse(rdyCode.begin(), rdyCode.end());
		dictionary[rdyCode] = chr;
	}

	/*auto it = dictionary.begin();
	while (it != dictionary.end()) {
		cout << it->first << " " << it->second << endl;
		it++;
	}*/
	
	string allInput = "";
	while (!archive.eof()) {
		unsigned char buff = archive.get();
		string temp = "";
		for (int i = 0; i < 8; i++) {
			temp.push_back(((buff | 1u) == buff) ? '1' : '0');
			buff >>= 1;
		}
		reverse(temp.begin(), temp.end());
		allInput.append(temp);
	}
		

	archive.close();
	archive.open("C:\\Users\\podus\\source\\repos\\WinRarOnMinimalkah\\WinRarOnMinimalkah\\output.sosi_jepy", ios::binary);
	//THIS SHIT LET ME TAKE THE LAST BYTE OF THE FILE
	archive.seekg(0, ios_base::end);
	int length = archive.tellg();
	archive.seekg(length - 1, ios_base::beg);
	int howManyZerosInTheEnd;	
	howManyZerosInTheEnd = archive.get();
	//-----------------------------------------------
	for (int i = 0; i < 8 + 8 + ((howManyZerosInTheEnd == 0) ? 8 : howManyZerosInTheEnd); i++)
		allInput.pop_back();

	archive.close();
	cout << endl;
	
	/*for (int i = 0; i < allInput.size(); i++) {
		if (i % 8 == 0)
			cout << " ";
		cout << allInput[i];
	}
	cout << endl;*/

	vector<pair<string,char>> vec(dictionary.begin(), dictionary.end());
	sort(vec.begin(), vec.end(), myMapCmp);

	for (auto p : vec) {
		cout << p.first << " " << p.second << endl;
	}

	int off = 0;
	int strSize = 0;
	
	ofstream file("output_opened.txt");

	while (!allInput.empty()) {
		for (int i = 0; i < vec.size(); i++) {
			strSize = vec[i].first.size();
			//cout << allInput.substr(off, strSize) << endl << vec[i].first << endl;
			if (allInput.substr(off, strSize) == vec[i].first) {
				//cout << vec[i].second;
				file << vec[i].second;
				allInput.erase(off, strSize);
			}
		}
	}
	
	file.close();

	system("pause");
}