#include "order.h"


Order::Order(std::string _n, bool _c) :
	name(_n), category(_c), id(++i) {};

void Order::print() {
	std::cout << std::setw(3) << id << '\t'
		<< std::setw(13) << name << '\t'
		<< ((category) ? "high" : "low") << '\n';
}

int Order::i = 0;