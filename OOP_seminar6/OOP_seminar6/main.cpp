#define HIGH_PRIO true
#define LOW_PRIO false

#include <iostream>
#include <queue>
#include <string>
#include <iomanip>
#include <conio.h>

#include "myqueue.h"
#include "order.h"

int main() {
	myQueue orders;
	orders.push(Order("Multicooker", HIGH_PRIO));
	orders.push(Order("Iron", LOW_PRIO));
	orders.push(Order("iPhone", HIGH_PRIO));

	orders.setOrder(2, "Headphones", LOW_PRIO);
	orders.printQueue();

	myQueue orders2;
	orders.push(Order("Ice cream", LOW_PRIO));
	orders.push(Order("Pillow", LOW_PRIO));
	orders.push(Order("Mersedes", HIGH_PRIO));

	orders = orders + orders2;
	orders.printQueue();

	orders.sortByPriority();
	orders.printQueue();

	_getch();
	//system("pause");
}