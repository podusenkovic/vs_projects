#include "myqueue.h"

myQueue::myQueue() {
	data = std::queue<Order>();
}

void myQueue::push(Order a) {
	data.push(a);
}

void myQueue::printQueue() {
	std::queue<Order> temp = data;
	while (!temp.empty()) {
		temp.front().print();
		temp.pop();
	}
	std::cout << '\n';
}

void myQueue::setOrder(int id, std::string n, bool cat) {
	std::queue<Order> temp = data;
	data = std::queue<Order>();

	while (!temp.empty()) {
		if (temp.front().id == id) {
			temp.front().category = cat;
			temp.front().name = n;
		}
		data.push(temp.front());
		temp.pop();
	}
}

myQueue& myQueue::operator+(const myQueue& a) {
	static myQueue temp;
	std::queue<Order> in1 = data;
	std::queue<Order> in2 = a.data;
	while (!in1.empty()) {
		temp.push(in1.front());
		in1.pop();
	}
	while (!in2.empty()) {
		temp.push(in2.front());
		in2.pop();
	}
	return temp;
}

void myQueue::sortByPriority() {
	std::queue<Order> output;
	std::queue<Order> in = data;
	while (!in.empty()) {
		if (in.front().category == HIGH_PRIO)
			output.push(in.front());
		in.pop();
	}
	in = data;
	while (!in.empty()) {
		if (in.front().category == LOW_PRIO)
			output.push(in.front());
		in.pop();
	}
	data = output;
}