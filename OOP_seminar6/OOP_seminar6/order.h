#pragma once
#include <iostream>
#include <string>
#include <iomanip>

class Order {
	friend class myQueue;

	static int i;
	int id;
	bool category;
	std::string name;
public:
	Order(std::string _n, bool _c);
	void print();
};