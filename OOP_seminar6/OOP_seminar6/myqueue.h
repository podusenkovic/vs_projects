#pragma once

#define HIGH_PRIO true
#define LOW_PRIO false

#include <iostream>
#include <string>
#include <iomanip>
#include <queue>
#include "order.h"

class myQueue {
	std::queue<Order> data;
public:
	myQueue();
	void push(Order a);
	void printQueue();
	void setOrder(int id, std::string n, bool cat);
	myQueue& operator+(const myQueue& a);
	void sortByPriority();
};

