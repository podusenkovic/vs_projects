﻿using System;
using System.Collections.Generic;
using System.IO;

namespace momsTestParser
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader questions = new StreamReader(@"C:\Users\podus\Desktop\questions.txt");
            StreamReader answers = new StreamReader(@"C:\Users\podus\Desktop\answers.txt");
            StreamWriter result = new StreamWriter(@"C:\Users\podus\Desktop\result.txt");

            Dictionary<string, string> q_num_str = new Dictionary<string, string>();

            while (!answers.EndOfStream)
            {
                string line_q = " ";
                do
                {
                    line_q = questions.ReadLine();
                } while (line_q[1] == ')');
                string line_a = answers.ReadLine();
                Console.WriteLine(line_a + " :: " + line_q);
                int skip_lanes_count = Convert.ToInt32(line_a.Split(' ')[1][0] - 'а');
                string line_res = "";
                line_res += line_q;
                while (skip_lanes_count >= 0)
                {
                    skip_lanes_count--;
                    line_q = questions.ReadLine();
                }
                line_res += "\n" + line_q;
                result.WriteLine(line_res);
            }


            questions.Close();
            answers.Close();
            result.Close();
            Console.WriteLine("Hello World!");
        }
    }
}
