#pragma once
#include <iostream>
#include <windows.h>

using namespace std;

void cursor(short x, short y) {
	COORD c;
	c.X = x;
	c.Y = y;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
}

class Lego {
protected:
	int width, height, depth;
	int x, y;
public:
	Lego(int _w, int _h, int _d, int _x, int _y) :
		width(_w), height(_h), depth(_d),
		x(_x), y(_y) {};
	
	virtual void draw() = 0;
};

class Rect : public Lego {
public:
	Rect(int _w, int _h, int _d, int _x, int _y) :
		Lego(_w, _h, _d, _x, _y) {};

	void draw() {
		cursor(x, y);
		for (int i = 0; i < width; i++)
			cout << "-";
		cursor(x, y);
		for (int i = 0; i < width; i++) {
			cout << "-";
		}
	}
	
};