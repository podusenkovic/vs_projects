#include <iostream>

using namespace std;

int main() {
	char char_min, char_max;
	int int_min, int_max;
	long long_min, long_max;
	long long llong_min, llong_max;

	unsigned char uchar_min, uchar_max;
	unsigned int uint_min, uint_max;
	unsigned long ulong_min, ulong_max;
	unsigned long long ullong_min, ullong_max;

	
	// signed
	char_max &= (0u << (sizeof(char) * 8 - 1));
	for (int i = 0; i < sizeof(char) * 8 - 1; i++) {
		char_min &= (0u << i);
		char_max |= (1u << i);
	}
	char_min |= (1u << (sizeof(char) * 8 - 1));

	int_max &= (0u << (sizeof(int) * 8 - 1));
	for (int i = 0; i < sizeof(int) * 8 - 1; i++) {
		int_min &= (0u << i);
		int_max |= (1u << i);
	}
	int_min |= (1u << (sizeof(int) * 8 - 1));

	long_max &= (0u << (sizeof(long) * 8 - 1));
	for (int i = 0; i < sizeof(long) * 8 - 1; i++) {
		long_min &= (0u << i);			
		long_max |= (1u << i);			
	}
	long_min |= (1u << (sizeof(long) * 8 - 1));
//------------------------------------------------------------------------------------------------
	llong_max &= (0u << (sizeof(long long) * 8 - 1));			// �� ��������� �������� ����� ����.
	for (int i = 0; i < sizeof(long long) * 8 - 1; i++) {		// �� ����������� ��������, ���
		llong_min &= (0u << i);									// llong_min = 10..0 (63 ����)
		llong_max |= (1u << i);									// llong_max = 01..1 (63 �������)
	}															// ������ �� ������ ������ ����
	llong_min |= (1u << (sizeof(long long) * 8 - 1));			//			:C
//------------------------------------------------------------------------------------------------
	// unsigned
	for (int i = 0; i < sizeof(unsigned char) * 8 - 1; i++) {
		uchar_min &= (0u << i);
		uchar_max |= (1u << i);
	}

	for (int i = 0; i < sizeof(unsigned int) * 8 - 1; i++) {
		uint_min &= (0u << i);
		uint_max |= (1u << i);
	}

	for (int i = 0; i < sizeof(unsigned long) * 8 - 1; i++) {
		ulong_min &= (0u << i);
		ulong_max |= (1u << i);
	}

	for (int i = 0; i < sizeof(unsigned long long) * 8 - 1; i++) {
		ullong_min &= (0u << i);
		ullong_max |= (1u << i);
	}
	
	
	printf("     data type     bytes          min value           max value  \n");
	printf("-----------------------------------------------------------------\n");

	printf("       char         %4d           %6d             %6d  \n", sizeof(char), char_min, char_max);
	printf("        int         %4d          %6d         %6d  \n", sizeof(int), int_min, int_max);
	printf("   long int         %4d          %6ld         %6ld  \n", sizeof(long int), long_min, long_max); // � �64 ����� ��� long long int, � �32 ��� int
	printf("   long long int    %4d          %lld        %lld  \n", sizeof(long long int), llong_min, llong_max); 
	
	printf("\n\n");
	
	printf("     u char         %4d          %6u              %6u  \n", sizeof(unsigned char), uchar_min, uchar_max);
	printf("     u  int         %4d          %6u              %6u  \n", sizeof(unsigned int), uint_min, uint_max);
	printf(" u long int         %4d          %6u              %6u  \n", sizeof(unsigned long int), ulong_min, ulong_max); // � �64 ����� ��� long long int, � �32 ��� int
	printf(" u long long int    %4d               %u          %llu  \n", sizeof(unsigned long long int), ullong_min, ullong_max);
	
	printf("\n\n");
	
	system("pause");
	return 0;

}