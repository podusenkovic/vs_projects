"""Описание модуля.

Подробное описание использования.
"""

import sys
import getopt
import serial

def main():
    # Разбираем аргументы командной строки
    try:
        opts, args = getopt.getopt(sys.argv[1:], "h", ["help"])
    except getopt.error as msg:
        print (msg)
        print ("для справки используйте --help")
        sys.exit(2)
    # process options
    for o, a in opts:
        if o in ("-h", "--help"):
            print (__doc__);
            sys.exit(0)
    # Анализируем
    for arg in args:
        process(arg) # process() определен в другом месте

if __name__ == "__main__":
    main()
