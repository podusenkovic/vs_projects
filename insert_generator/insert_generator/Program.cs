﻿using System;
using System.Collections.Generic;
using System.IO;

namespace insert_generator
{
    class Program
    {
        static string genString(int size)
        {
            Random r = new Random();
            string a = "" + (char)(r.Next(26) + 'A');
            for (int i = 1; i < size; i++)
                a += (char)(r.Next(26) + 'a');
            return a;
        }

        static string genNumString(int size)
        {
            Random r = new Random();
            string a = "";
            for (int i = 0; i < size; i++)
                a += (char)(r.Next(10) + '0');
            return a;
        }

        static void Main(string[] args)
        {
            Random r = new Random();
            List<string> sobs = new List<string>();
            List<string> techs = new List<string>();
            StreamReader rd = new StreamReader("techs.txt");
            while (!rd.EndOfStream)
            {
                techs.Add(rd.ReadLine());
            }
            rd.Close();

            rd = new StreamReader("passports.txt");
            while (!rd.EndOfStream)
            {
                sobs.Add(rd.ReadLine());
            }
            rd.Close();


            List<string> states = new List<string>();
            states.Add("Готово");
            states.Add("В процессе");
            states.Add("Отклонено");
            states.Add("На обсуждении");
            StreamWriter wr = new StreamWriter("result.txt");
            for (int i = 0; i < 100; i++)
            {
                string rslt = "( ";
                /*rslt += "'" + DateTime.Now.AddDays(-r.Next(10000)).ToShortDateString() + "'" + ", ";
                rslt += r.Next(10, 32000) + ", ";
                rslt += r.Next(1, 1000) + ", ";
                rslt += r.Next(1, 100) + ", ";
                rslt += "'" + genString(r.Next(4, 100)) + "'" + ", ";
                rslt += "'" + genNumString(2) + ":" + genNumString(2) + ":" + genNumString(6) + ":" + genNumString(2) + "'" + ", ";
                rslt += "'" + genNumString(2) + ":" + genNumString(3) + ":" + genNumString(3) + ":" + genNumString(9) + ":" + genNumString(4) + "'" + ", ";
                rslt += r.Next(1, int.MaxValue) + ", ";
                rslt += "'" + genString(r.Next(4, 15)) + "'" + ", ";
                rslt += "'" + states[r.Next(states.Count)] + "'" + "),";*/

                /*rslt += "'" + genString(r.Next(4, 20)) + genString(r.Next(4, 20)) + genString(r.Next(4, 20)) + "'" + ", ";
                rslt += "'" + DateTime.Now.AddDays(-r.Next(40000)).ToShortDateString() + "'" + ", ";
                rslt += "'" + genNumString(4) + "-№" + genNumString(6) + "'" + ", ";
                rslt += "'" + genString(r.Next(7, 30)) + "'" + ", ";
                rslt += "'" + genString(r.Next(10, 150)) + "'" + "), ";
                wr.WriteLine(rslt);*/
            }

            for(int i = 0; i < sobs.Count; i++)
            {
                string rslt = "( ";
                int index = r.Next(techs.Count);
                rslt += "'" + sobs[i] + "'" + ", " + "'" + techs[index] + "'" + "),";
                if (i % 2 == 0) techs.RemoveAt(index);
                wr.WriteLine(rslt);
            }
            for (int i = 0; i < techs.Count; i++)
            {
                string rslt = "( ";
                int index = r.Next(sobs.Count);
                rslt += "'" + sobs[index] + "'" + ", " + "'" + techs[i] + "'" + "),";
                wr.WriteLine(rslt);
            }

            wr.Close();
        }
    }
}
