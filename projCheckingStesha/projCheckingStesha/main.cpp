#include<stdio.h>
#include<iostream>
#include<cstdlib>
#include<time.h>
#include<vector>
#include<string>
#include <iomanip>
using namespace std;

class Data {
private:
	vector<string> A;
	vector<string> B;
	vector<int> S;
	int ** C;
	int h;
	int w;
public:
	Data() {
		h = 0;
		w = 0;
	}
	void add_student(string s)
	{
		h++;
		A.push_back(s);
	}
	void add_subject(string s)
	{
		w++;
		B.push_back(s);
	}
	void generate_marks()
	{
		C = new int*[w];
		for (int i = 0; i < w; i++)
			C[i] = new int[h];
		for (int i = 0; i < h; i++)
			for (int j = 0; j < w; j++)
				C[i][j] = rand() % 4 + 2;
	}
	void show_data()
	{
		cout << "surname  ";
		for (int i = 0; i < w; i++)
			cout << setw(10) << B[i] << "  ";
		cout << endl;
		for (int i = 0; i < h; i++)
		{
			cout << setw(8) << A[i] << "  ";
			for (int j = 0; j < w; j++)
			{
				cout << setw(10) << C[i][j] << "  ";
			}
			cout << endl;
		}
	}
	void show_task()
	{
		int s;
		for (int i = 0; i < h; i++)
		{
			s = 0;
			for (int j = 0; j < w; j++)
			{
				s += C[i][j];
			}
			S.push_back(s);
		}
		for (int i = 0; i < h; i++)
			cout << setw(4) << S[i] << endl;

		int k;
		string f;
		for (int i = 0; i<(h - 1); i++)
		{
			int z = i + 1;
			while (z < h)
			{
				if (S[i] < S[z])
				{
					k = S[i];
					S[i] = S[z];
					S[z] = k;

					f = A[i];
					A[i] = A[z];
					A[z] = f;
					for (int j = 0; j < w; j++)
					{
						k = C[i][j];
						C[i][j] = C[z][j];
						C[z][j] = k;
					}
				}
				z++;
			}
		}
	}
	~Data() {
		for (int i = 0; i < w; i++)
			delete[]C[i];
		delete[]C;

	}
};
int main() {
	srand(time(0));
	Data table;

	cout << "Exam Task Kharitonova (Variant1.3)" << endl;
	bool flag = false;
	bool check1 = false;
	bool check2 = false;
	bool check3 = false;
	do {
		int k;
		cout << "enter:" << endl;
		cout << "1-enter students surnames" << endl;
		cout << "2-enter subjects" << endl;
		cout << "3-generate marks" << endl;
		cout << "4-show data" << endl;
		cout << "5-show task" << endl;
		cout << "6-exit" << endl;
		cin >> k;
		switch (k) {
		case(1):
		{
			bool flag1 = false;
			int k1;
			do {
				cout << "enter:" << endl;
				cout << "1-add surname" << endl;
				cout << "2-exit" << endl;
				cin >> k1;
				switch (k1)
				{
				case(1):
				{
					string s;
					cin >> s;
					table.add_student(s);
					break;
				}
				case(2):
				{
					flag1 = true;
					break;
				}
				}
			} while (flag1 == false);
			check1 = true;
			break;
		}
		case(2):
		{
			int n = 0;
			string s;
			while (n < 5)
			{
				cout << "enter subject" << endl;
				cin >> s;
				table.add_subject(s);
				n++;
			}
			check2 = true;
			break;
		}
		case(3):
		{
			if (check1 == true && check2 == true)
			{
				table.generate_marks();
				cout << "marks generated" << endl;
				check3 = true;
			}
			else cout << "unable to generate marks" << endl;
			break;
		}
		case(4):
		{
			if (check1 == true && check2 == true)
			{
				table.show_data();
			}
			else cout << "unable to show tables" << endl;
			break;
		}
		case(5):
		{
			if (check1 == true && check2 == true && check3 == true)
			{
				table.show_task();
				table.show_data();
			}
			else cout << "unable to show the task" << endl;
			break;
		}
		case(6):
		{
			flag = true;
			break;
		}
		}
	} while (flag == false);


	getchar();
	getchar();
	return 0;
}