﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace DB_Lab7
{
    public partial class Form1 : Form
    {
        OleDbConnection cn;
        OleDbCommand cmd;
        public Form1()
        {
            cn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.16.0;
                                       Data Source=C:\\LAB4.accdb");
            cmd = new OleDbCommand();
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cmd.Connection = cn;
        }

        private void Form1_Resize(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            cmd.CommandText = "SELECT * FROM Продажа";
            var rd = cmd.ExecuteReader();
            for(int i = 0; i < rd.FieldCount; i++)
            {
                DataGridViewColumn val = new DataGridViewColumn();
                val.Name = rd.GetName(i);
                dataGridView1.Columns.Add(val);
            }
        }
    }
}
