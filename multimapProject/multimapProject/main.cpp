#define BLANK "                                                 " // ����������� (�����)
#define KEY_ESC 27

#include <conio.h>

#include <Windows.h>
#include <iostream>
#include <fstream>
#include <map>
#include <algorithm>
#include <vector>
#include <string>

using namespace std;

/*vector<char> allowedSymbols = {'(', ')', KEY_ESC}; // ����� �������� �������, ������� ����� ������������ � ��������� ������

void hideCursor(){			// ������� ���������� ��������� �������
	HANDLE consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_CURSOR_INFO info;
	info.dwSize = 100;
	info.bVisible = FALSE;
	SetConsoleCursorInfo(consoleHandle, &info);
}

bool isEnglish(const string &test) { // �������� �� ��, �� ���������� �� �����
	for (auto c : test)
		if ((c < 'A' || c > 'Z') && (c < 'a' || c > 'z') && find(allowedSymbols.begin(), allowedSymbols.end(), c) == allowedSymbols.end())
			return false;
	return true;
}

bool isRussian(const string &test) { // �������� �� ��, �� ������� �� �����
	for (auto c : test)
		if ((c < '�' || c > '�') && (c < '�' || c > '�') && find(allowedSymbols.begin(), allowedSymbols.end(), c) == allowedSymbols.end())
			return false;
	return true;
}

void printDict(const multimap<string, string> &dict) { // ������� ������ ������� �� �����
	auto it = dict.begin();
	while (it != dict.end()) {
		cout << it->first << " " << it->second << endl;
		it++;
	}
}

void setPos(short int x, short int y) {	// ������� ��� ����������� ������� � �������
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), { x, y });
}

void cleanScreen() {	// ������� ���� �������(false) / ������� ������ ����������� �������� (true)
	for (int i = 0; i <= 40; i++) {
		setPos(0, i);
		cout << BLANK;
	}
	setPos(0, 0);
}

void main() {
	hideCursor();
	setlocale(LC_ALL, "Russian");	// ��� �������� ����� � �������
	system("chcp 1251 > nul");		// ��� ����� ������� ���� � �������

	// �������� multimap
	multimap<string, string> dictionaryENGtoRUS;   // �����-�������
	multimap<string, string> dictionaryRUStoENG;   // ������-����������

	ifstream inputDict("dictionary.txt");	// �������� ����� �� ������
	if (!inputDict.is_open()) {	// �������� ������������ �������� �����
		cout << "error";
		Sleep(1000);
		return;
	}

	string buf1, buf2;
	while (!inputDict.eof()) {					// ������ ������ � �����
		inputDict >> buf1 >> buf2;
		if (isEnglish(buf1) && isRussian(buf2)) {
			dictionaryENGtoRUS.insert(pair<string, string>(buf1, buf2));
			dictionaryRUStoENG.insert(pair<string, string>(buf2, buf1));
		}
		else if (isEnglish(buf2) && isRussian(buf1)) {
			dictionaryENGtoRUS.insert(pair<string, string>(buf2, buf1));
			dictionaryRUStoENG.insert(pair<string, string>(buf1, buf2));
		}
		else cout << "bad input string\n";
	}
	inputDict.close();

	// ���������� ����
	short int choice;
	do {
		cout << "\n				Menu	\n";
		cout << "	1. Open translater		\n";
		cout << "	2. Show dictionary		\n";
		cout << "	3. Add a new word		\n";
		cout << "	4. Save updated dict	\n";
		cout << "	5. EXIT					\n";
		choice = _getch() - '0';		// �������� ��������� ��������
		cleanScreen();					// ������� ����� �� �������
		switch (choice) {
		case 1:
			do {
				static string inputStr;		// ������ �������� �������� �������
				int offersAmount = 0;		// ���������� ��������� ���-�� ���������� ���������)
				unsigned char symb = _getch();		// ������ �������
				cleanScreen();		// ������� ����� �� �������
				inputStr.push_back(symb);	// ��������� ������ � ������
				if (!isEnglish(inputStr) && !isRussian(inputStr)) {
					inputStr.pop_back();
					continue;
				}
				cout << inputStr;			// �� _getch() �� ������� �� ����� �������� ������, ������� ������� ���� ������		
				if (symb == KEY_ESC) {			// ���� ����� ESC, �� ��������� �������, ������� ����� � ������
					cleanScreen();
					inputStr.clear();
					break;
				}
				else if (isEnglish(inputStr)) {
					auto it = dictionaryENGtoRUS.begin();		// �������� �� ������� ��� ������ ����������
					while (it != dictionaryENGtoRUS.end()) {
						if (it->first.substr(0, inputStr.size()) == inputStr) {	// ���������� ����� ������ � ���, ��� �� �����
							offersAmount++;				// ���� ��� �����, �� ����������� ���-�� �����������
							setPos(0, offersAmount);	// ������� ������ ����
							cout << it->first << " " << it->second;			// ������� �����������
						}
						it++;
					}
				}
				else if (isRussian(inputStr)) {
					auto it = dictionaryRUStoENG.begin();		// �������� �� ������� ��� ������ ����������
					while (it != dictionaryRUStoENG.end()) {
						if (it->first.substr(0, inputStr.size()) == inputStr) {	// ���������� ����� ������ � ���, ��� �� �����
							offersAmount++;				// ���� ��� �����, �� ����������� ���-�� �����������
							setPos(0, offersAmount);	// ������� ������ ����
							cout << it->first << " " << it->second;			// ������� �����������
						}
						it++;
					}
				}
				if (offersAmount == 0) {		// ���� �� ������� �� ������ ���������, �� ������� ���������� �� ����
					setPos(0, 1);	// ������� ������ ����
					cout << "no translate for this";			// ������� ���������� �� ��������� �����
				}
			} while (true);
			break;
		case 2:
			printDict(dictionaryENGtoRUS);
			_getch();
			cleanScreen();
			break;
		case 3:{
			cout << "Input words with a blank between : \n";
			string buf1, buf2;
			cin >> buf1 >> buf2;
			if (isEnglish(buf1) && isRussian(buf2)) {
				dictionaryENGtoRUS.insert(pair<string, string>(buf1, buf2));
				dictionaryRUStoENG.insert(pair<string, string>(buf2, buf1));
			}
			else if (isEnglish(buf2) && isRussian(buf1)) {
				dictionaryENGtoRUS.insert(pair<string, string>(buf2, buf1));
				dictionaryRUStoENG.insert(pair<string, string>(buf1, buf2));
			}
			else cout << "bad input string\n";
			break;
		}
		case 4:
			ofstream outputDict("new_dictionary.txt");
			auto it = dictionaryENGtoRUS.begin();
			while (it != dictionaryENGtoRUS.end()) {
				outputDict << it->first << " " << it->second << endl;
				it++;
			}
			outputDict.close();
		}
	} while (choice != 5);
}*/


void main() {


}