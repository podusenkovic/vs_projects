#pragma once

#include "database.h"
#include "worker.h"

#include <iostream>
#include <vector>
#include <string>
#include <map>

using namespace std;

class Admin
{
	string name;
	Database db;
public:
	Admin(string name, Database &db);
	void letWorkerAccess(Worker &w, int office);
	void denayWorkerAccess(Worker &w, int office);
};

