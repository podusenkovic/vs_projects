#pragma once
#include "database.h"

#include <iostream>
#include <vector>
#include <string>
#include <map>

using namespace std;

class Worker {
	friend class Admin;
	string department;
	string name;
	vector<bool> access;
	vector<int> offices;
public:
	Worker(string name, string department, Database &db);
	void printData();
};