#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <map>
#include <time.h>

#include "database.h"
#include "worker.h"
#include "admin.h"

using namespace std;


int main() {
	srand(time(0));
	Database db("database.txt");

	db.printDB();
	cout << endl;
	Worker worker1("Dimon", "HR", db);
	worker1.printData();
	cout << endl;

	Admin SisAdmin("Anton", db);
	SisAdmin.denayWorkerAccess(worker1, 105);

	worker1.printData();
	cout << endl;
	
	vector<bool> test;
	for (int i = 0; i < db.getOfficesList().size(); i++)
		test.push_back(rand() % 2);

	db.addNewDepartment("Cooks", test);

	db.printDB();
	//db.updateFile();

	system("pause");
}