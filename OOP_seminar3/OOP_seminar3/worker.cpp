#include "worker.h"

Worker::Worker(string name, string department, Database &db) {
	this->name = name;
	this->department = department;
	access = db.getAccessList(department);
	offices = db.getOfficesList();
}

void Worker::printData() {
	cout << "My name is " << name << endl;
	cout << "I'm working in " << department << endl;
	cout << "And i have ability to get into these rooms:" << endl;
	for (int i = 0; i < access.size(); i++) {
		(access[i]) ? cout << offices[i] << "  " : 
					  cout << "";
	}
	cout << endl;
}
