#include "admin.h"

Admin::Admin(string name, Database &db){
	this->name = name;
	this->db = db;
}

void Admin::letWorkerAccess(Worker &w, int office) {
	string dep = w.department;
	for (int i = 0; i < db.offices.size(); i++)
		if (db.offices[i] == office) {
			int index_to_change = i;
			w.access[index_to_change] = true;
			return;
		}
}

void Admin::denayWorkerAccess(Worker &w, int office) {
	string dep = w.department;
	for (int i = 0; i < db.offices.size(); i++)
		if (db.offices[i] == office) {
			int index_to_change = i;
			w.access[index_to_change] = false;
			return;
		}
}


