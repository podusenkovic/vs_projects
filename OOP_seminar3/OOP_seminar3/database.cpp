#include "database.h"

Database::Database(string filename) {
	ifstream file(filename);
	int depsize, offsize;
	file >> depsize >> offsize;
	for (int i = 0; i < offsize; i++) {
		int buff;
		file >> buff;
		offices.push_back(buff);
	}
	for (int i = 0; i < depsize; i++) {
		string dep;
		file >> dep;
		departments.push_back(dep);
		for (int j = 0; j < offsize; j++) {
			int buff;
			file >> buff;
			access[dep].push_back(buff);
		}
	}
	file.close();
}


void Database::printDB() {
	for (int i = 0; i < offices.size(); i++)
		cout << "\t" << offices[i];
	cout << endl;
	for (int i = 0; i < departments.size(); i++) {
		cout << departments[i];
		for (int j = 0; j < offices.size(); j++)
			cout << "\t " << access[departments[i]][j];
		cout << endl;
	}
}

void Database::changeAccess(string dep, int officeNum, bool to_put) {
	for (int i = 0; i < offices.size(); i++)
		if (offices[i] == officeNum) {
			int index_to_change = i;
			access[dep][index_to_change] = to_put;
			return;
		}
}

vector<bool> Database::getAccessList(string dep) {
	return access[dep];
}

vector<int> Database::getOfficesList(){
	return offices;
}

void Database::addNewOffice(int office, vector<bool> accessToIt) {
	offices.push_back(office);
	for (int i = 0; i < departments.size(); i++)
		access[departments[i]].push_back(accessToIt[i]);
}

void Database::addNewDepartment(string dep, vector<bool> accessToIt) {
	departments.push_back(dep);
	for (int i = 0; i < offices.size(); i++)
		access[dep].push_back(accessToIt[i]);
}

void Database::updateFile() {
	ofstream file("database.txt");
	file << departments.size() << "\t"
		 << offices.size() << endl;
	for (int i = 0; i < offices.size(); i++)
		file << "\t" << offices[i];
	for (int i = 0; i < departments.size(); i++) {
		file << endl << departments[i];
		for (int j = 0; j < offices.size(); j++)
			file << "\t " << access[departments[i]][j];
	}
	file.close();
}