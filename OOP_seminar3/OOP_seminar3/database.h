#pragma once

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <map>
#include <algorithm>

using namespace std;

class Database {
	friend class Admin;

	vector<int> offices;
	vector<string> departments;
	map<string, vector<bool>> access;
public:
	Database(string filename = "database.txt");
	void printDB();
	void changeAccess(string dep, int officeNum, bool to_put);
	vector<bool> getAccessList(string dep);
	vector<int> getOfficesList();
	void addNewOffice(int office, vector<bool> accessToIt);
	void addNewDepartment(string dep, vector<bool> accessToIt);
	void updateFile();

};
