#include <iostream>
#include <conio.h>
#include <vector>
#include <algorithm>
#include <math.h>
#include <sstream>
#include <iterator>

typedef unsigned long long int u64;

using namespace std;

#define EPS 10E-5

/*int main() {
	int n = 0;
	cin >> n;
	vector<unsigned int> results;
	for (int i = 0; i < n; i++) {
		int curr_pow = 0;
		cin >> curr_pow;
		unsigned int result = 0;
		result = pow((double)2.0, curr_pow);
		for (int j = 1; j < curr_pow / 2; j++) {
			result += pow((double)2.0, j);
		}
		for (int j = curr_pow / 2; j < curr_pow; j++) {
			result -= pow((double)2.0, j);
		}
		results.push_back(result);
	}
	for_each(results.begin(), results.end(), [](int a) {cout << a << endl; });
}*/

int main() {
	int t = 0;
	cin >> t;
	vector<u64> results;
	for (int i = 0; i < t; i++) {
		u64 n = 0, x = 0;
		cin >> n;
		for (x = 1; x <= n; x++) {
			if (n % x != 0)
				continue;
			u64 val = n / x + 1;
			while (val % 2 ) {
				val /= 2;
			}
			
		}
		results.push_back(x);
	}
	for_each(results.begin(), results.end(), [](u64 a) {cout << a << endl; });
}