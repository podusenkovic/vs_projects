#include <iostream> 
#include <fstream> 

using namespace std;
bool IsPrime(int n) {
	int i;
	for (i = 2; i < n / 2; i++) {
		if (n % i == 0) return false;
	}
	return true;
}

int main()
{
	ifstream fin("input.txt");
	ofstream fout("output.txt");
	int n, c = 0;
	fin >> n;
	cout << n;
	for (int i = n; i < 2*n; i++) {
		if (IsPrime(i))
			c++;
	}
	fout << c;
	fin.close();
	fout.close();
	return 0;
}