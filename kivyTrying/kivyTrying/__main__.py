from kivy.app import App
from kivy.uix.widget import Widget
from kivy.properties import NumericProperty, ReferenceListProperty, ObjectProperty
from kivy.vector import *
from kivy.clock import Clock
from random import randint

class PongBall(Widget):
    velocity_x = NumericProperty(0)
    velocity_y = NumericProperty(0)

    velocity = ReferenceListProperty(velocity_x, velocity_y)

    def move(self):
        self.pos = Vector(*self.velocity) + self.pos

class PongGame(Widget):
    ball = ObjectProperty(None);
    def serve_balls(self):
        self.ball.center = self.center;
    


class PongApp(App):
    def build(self):
        game = PongGame()
        game.serve_ball()
        Clock.shedule_interval()
        return PongGame()

if __name__ == '__main__':
    PongApp().run()


