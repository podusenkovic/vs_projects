from cx_Freeze import setup, Executable
import os.path

base = None

PYTHON_INSTALL_DIR = r'C:\Users\podus\AppData\Local\Programs\Python\Python36'
executables = [Executable("qrCodesProj.py", base=base)]

packages = ["idna"]
options = {
    'build_exe': {
		 'include_files':[
            os.path.join(PYTHON_INSTALL_DIR, 'DLLs', 'tk86t.dll'),
            os.path.join(PYTHON_INSTALL_DIR, 'DLLs', 'tcl86t.dll'),
         ],
        'packages':packages,
    },

}

setup(
    name = "program",
    options = options,
    version = "1",
    description = 'description',
    executables = executables
)