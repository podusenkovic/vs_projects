#from pip._vendor import request

#import PyInstaller
from tkinter import *
import zipfile
import urllib.request
import io

# coding: cp1251
WIDTH = 600
HEIGHT = 600


root = Tk()
root.title("title")
root.geometry("{}x{}".format(HEIGHT, WIDTH))

def foo():
    photo = PhotoImage(file="qr-code.png")
    photo = photo.zoom(2, 2)

    photo_label = Label(root, image=photo)  
    photo_label.image = photo   
    photo_label.grid(row = 5, column = 0, columnspan = 10)


def createFile():
    site = open('index.html', 'w')
    site.write('<html>\n')
    site.write('<head>\n')
    site.write('<title>Data card</title>\n')
    site.write('</head>\n')
    site.write('<body>\n')
    site.write('<p>Имя : {}\n'.format(nameEntry.get()))
    site.write('<p>Фамилия : {}\n'.format(secNameEntry.get()))
    site.write('<p>Организация : {}\n'.format(organizEntry.get()))
    site.write('<p>Телефон : {}\n'.format(phoneEntry.get()))
    site.write('<p>Почта : {}\n'.format(mailEntry.get()))
    site.write('</body>\n')
    site.write('</html>')
    site.close()

def createZipFile():
    zFile = zipfile.ZipFile('site.zip','w')
    zFile.write('index.html', compress_type = zipfile.ZIP_DEFLATED)
    zFile.write('qr-code.gif', compress_type = zipfile.ZIP_DEFLATED)
    zFile.close()

def QRGET():
	S2 = 'http://qrcoder.ru/code/?' + urllib.parse.quote_plus(('Имя :' + nameEntry.get()).encode('cp1251')) + '+%0D%0A%0D%0A' + urllib.parse.quote_plus(('Фамилия :' + secNameEntry.get()).encode('cp1251')) + '+%0D%0A%0D%0A' + urllib.parse.quote_plus(('Организация :' + organizEntry.get()).encode('cp1251')) + '+%0D%0A%0D%0A' + urllib.parse.quote_plus(('Телефон :' + phoneEntry.get()).encode('cp1251')) + '+%0D%0A%0D%0A' + urllib.parse.quote_plus(('Почта :' + mailEntry.get()).encode('cp1251')) + '&4&0'
	print(S2)
	with urllib.request.urlopen(S2) as url:
		qr = url.read()
	f = open("qr-code.png", "wb")
	f.write(qr)
	f.close(); foo()	
	
	
	
	
createFileButton = Button(root, text="Create file", width = 10, command = createFile)
zipFilesButton = Button(root, text="Zip files", width = 10, command = createZipFile)
QRButton = Button(root, text="Get QR", width = 10, command = QRGET)

nameEntry = Entry(root)
secNameEntry = Entry(root)
organizEntry = Entry(root)
phoneEntry = Entry(root)
mailEntry = Entry(root) 

nameEntry.grid(row = 0, column = 0)
secNameEntry.grid(row = 1, column = 0)
organizEntry.grid(row = 2, column = 0)
phoneEntry.grid(row = 3, column = 0)
mailEntry.grid(row = 4, column = 0)
createFileButton.grid(row = 4, column = 1)
zipFilesButton.grid(row = 4, column = 2)
QRButton.grid(row = 4, column = 4)

root.mainloop()
