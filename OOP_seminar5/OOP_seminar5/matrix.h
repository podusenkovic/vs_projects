#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <iomanip>


class Matrix {
	std::vector<std::vector<double>> matr;
	int rows, cols;
public:
	Matrix(const Matrix &copy);
	Matrix(std::ifstream &file);

	void print();

	int gRows();
	int gCols();

	Matrix& operator+(const Matrix &b);

	Matrix& operator-(const Matrix &b);

	Matrix& operator*(const Matrix &b);

	Matrix& operator/(const Matrix &b);
};