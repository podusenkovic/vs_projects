#include "matrix.h"

Matrix::Matrix(const Matrix &copy) {
	this->cols = copy.cols;
	this->rows = copy.rows;
	this->matr = copy.matr;
}
Matrix::Matrix(std::ifstream &file) {
	std::string data;
	/*file >> rows >> cols;
	for (int i = 0; i < rows; i++) {
		matr.push_back(std::vector<int>());		// �� ���������� ������� ���������� ������� �� �����
		for (int j = 0; j < cols; j++) {		// �� ���������� ����� ������ �������� ������� �� �������
			int in;
			file >> in;
			matr[i].push_back(in);
		}
	}*/
	rows = 0;
	cols = 0;								// ���� ���������� �������
	char buff;								// ���������� ������� � ����� ��� ������� ��������� ��������
	int preCols = cols;
	while (!file.eof()) {
		file.get(buff);						 // ��������� ������� �� ����� ������
		matr.push_back(std::vector<double>());  // ��������� ����� ������ � �������
		while (file.peek() != '\n' && !file.eof()) { // ���� ���������� ������
			double in;
			file >> in;						// ��������� �����
			matr[rows].push_back(in); cols++; // �������� ��� �����, � ����������� ���-�� ��-��� � ������ ������ �� 1
			if (rows != 0 && cols > preCols) // ���� �� ������ �� ������ ������ �������, � ���-�� ��-��� �� ���������
				throw 20;					 // �� ��������� � ���������� ������, �� ������ ����������
		}
		if (cols == 0 || file.eof()) {		// ������� ��������� ���������� ������� (0 ����� � ������ ��� ����� �����)
			if (file.eof())
				rows++;
			cols = preCols;
			break;
		}
		preCols = cols;						// ���� ��� ����, �� ���������� ���������
		rows++;
		cols = 0;
	}
}

void Matrix::print() {					// ����� ������ ������� �� �����
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++) {
			std::cout << std::setw(6) << std::setprecision(2) << matr[i][j];
		}
		std::cout << '\n';
	}
	std::cout << '\n';
}

int Matrix::gRows() {
	return rows;
}
int Matrix::gCols() {
	return cols;
}

// ���������� ����������� �������� ��� ������

Matrix& Matrix::operator+(const Matrix &b) {
	static Matrix *temp(this);
	for (int i = 0; i < rows; i++)
		for (int j = 0; j < cols; j++)
			temp->matr[i][j] += b.matr[i][j];
	return *temp;
}

Matrix& Matrix::operator-(const Matrix &b) {
	static Matrix *temp(this);
	for (int i = 0; i < rows; i++)
		for (int j = 0; j < cols; j++)
			temp->matr[i][j] -= b.matr[i][j];
	return *temp;
}

Matrix& Matrix::operator*(const Matrix &b) {
	static Matrix *temp(this);
	for (int i = 0; i < rows; i++)
		for (int j = 0; j < cols; j++)
			temp->matr[i][j] *= b.matr[i][j];
	return *temp;
}

Matrix& Matrix::operator/(const Matrix &b) {
	static Matrix *temp(this);
	for (int i = 0; i < rows; i++)
		for (int j = 0; j < cols; j++)
			temp->matr[i][j] /= (b.matr[i][j] != 0) ? b.matr[i][j] : throw 30;
	return *temp;
}
