#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "matrix.h"

int main() {
	std::ifstream myFile("input.txt");
	std::string operations;

	getline(myFile, operations);
	int amount = operations.length() + 1;

	std::vector<Matrix*> matrixes;
	try {
		for (int i = 0; i < amount; i++) {
			matrixes.push_back(new Matrix(myFile));
		}
		for (int i = 1; i < amount; i++)
			if (matrixes[i]->gCols() != matrixes[0]->gCols() || matrixes[i]->gRows() != matrixes[0]->gRows())
				throw 10;

		Matrix result = *matrixes[0];
		for (int i = 1; i < amount; i++)
		{
			switch (operations[i - 1]) {
			case '+': result = result + *matrixes[i];
				break;
			case '-': result = result - *matrixes[i];
				break;
			case '*': result = result * *matrixes[i];
				break;
			case '/': result = result / *matrixes[i];
				break;
			}
		}
		result.print();

	}
	catch (int e) {
		switch (e) {
		case 10: std::cout << "There are different sizes of matrixes, check input" << '\n';
			break;
		case 20:
			std::cout << "There are bad sizes of string in matrix, check input" << '\n';
			break;
		case 30:
			std::cout << "There is dividing by zero, check input" << '\n';
			break;
		}
	}

	system("pause");
}