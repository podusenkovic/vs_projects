#include <fstream>
#include <string>
using namespace std;

int main() {
	ifstream file("input.txt");
	ofstream outFile("output.txt");
	string str;
	while (!file.eof()) {
		if (file.get() != '<')
			continue;
		else if (file.get() != 'i')
			continue;
		else if (file.get() != 'm')
			continue;
		else {
			char buff = file.get();
			do {
				buff = file.get();
				str.push_back(buff);
			} while (buff != '<');
			outFile << str;
			str.clear();
		}
	}
	file.close();
	outFile.close();

}