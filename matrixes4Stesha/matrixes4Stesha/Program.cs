﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace matrixes4Stesha
{
    class Program
    {
        static double[,] createMatrix(int n, int m)
        {
            Random rand = new Random();
            double[,] matrix = new double [n, m];
            for (int i = 0; i < n; i++)
                for (int j = 0; j < m; j++)
                    matrix[i, j] = rand.Next(100,999) * 0.1;
            return matrix;
        }

        static void printMatrix(double[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                    Console.Write("{0} ", matrix[i, j]);
                Console.WriteLine();
            }
        }

        static void printArray(int[] array)
        {
            for (int i = 0; i < array.GetLength(0); i++)
            {
                Console.Write("{0} ", array[i]);
            }
            Console.WriteLine();
        }


        static int[] createArray(double[,] matrix, double halfRazmah)
        {
            int arrSize = 0;
            int k = 0;
            for (int i = 0; i < matrix.GetLength(0); i++)
                for (int j = 0; j < matrix.GetLength(1); j++)
                    if (Math.Abs(matrix[i, j]) > halfRazmah)
                    {
                        arrSize++;
                        break;
                    }
            int[] array = new int[arrSize];
            for (int i = 0; i < matrix.GetLength(0); i++)
                for (int j = 0; j < matrix.GetLength(1); j++)
                    if (Math.Abs(matrix[i, j]) > halfRazmah)
                    {
                        array[k] = i;
                        k++;
                        break;
                    }
            return array;
        }

        static double findHalfOfRazmah(double[,] matrix)
        {
            double Max = matrix[0, 0];
            double Min = matrix[0, 0];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] >= Max)
                        Max = matrix[i, j];
                    if (matrix[i, j] <= Min)
                        Min = matrix[i, j];
                }
            }
            return (Max + Min) / 2;
        }

        static void Main(string[] args)
        {
            Console.Write("Input number of rows : ");
            int n = int.Parse(Console.ReadLine());
            Console.Write("Input number of cols : ");
            int m = int.Parse(Console.ReadLine());

            double[,] A = createMatrix(n, m);
            printMatrix(A);

            double polovinaRazmaha = findHalfOfRazmah(A);

            Console.WriteLine();
            int[] arr = createArray(A, polovinaRazmaha);

            printArray(arr);
            Console.WriteLine();

            Console.Read();
        }
    }
}
