﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Sockets;
using System.Net;

namespace chatForTwo
{
    // Класс сервера
    class Server
    {
        private Socket socket; // сокет для прослушки запросов на подключения
        private IPEndPoint ipPoint; // объект данных о сокете
        private Socket handleRequest; // сокет для обработки подключения
        public Server(IPAddress addr, int port) // конструктор
        {
            ipPoint = new IPEndPoint(addr, port);
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }
        public void startListening() // метод сервера для начала прослушивания
        {
            socket.Bind(ipPoint);
            socket.Listen(10); 
            handleRequest = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            while (!handleRequest.Connected)
            {
                handleRequest = socket.Accept();
            }
        }
        public void readDataFromClient() // метод принятия данных от клиента
        {
            while (true)
            {
                StringBuilder strBuilder = new StringBuilder(); // для преобразования байты -> строка
                int bytes = 0;
                byte[] data = new byte[256];
                do
                {
                    bytes = handleRequest.Receive(data);
                    strBuilder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                } while (handleRequest.Available > 0);
                if (strBuilder.ToString() == "exit") // если собеседник ввел exit, то выходим
                {
                    Console.WriteLine("\nСобеседник отключился!");
                    return;
                }
                else Console.WriteLine($"\nFrom client : {strBuilder.ToString()}"); // иначе выводим сообщение на экран
            }
        }
        public void sendDataToClient(string message) // метод отправки данных клиенту
        {
            byte[] data = Encoding.Unicode.GetBytes(message);
            handleRequest.Send(data);
        }

        public void disconnect() // метод для отключения сокетов
        {
            handleRequest.Shutdown(SocketShutdown.Both);
            handleRequest.Close();
            socket.Close();
        }
    }
    // класс Клиент
    class Client
    {
        private Socket socket; // у клиента достаточно одного сокета для передачи и принятия (без прослушки)
        private IPEndPoint ipPoint; // аналогично объект данных
        public Client(IPAddress addr, int port)
        {
            ipPoint = new IPEndPoint(addr, port);
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket.Connect(ipPoint); // сразу пытаемся подключиться
        }

        public void disconnect()
        {
            // закрываем сокет
            socket.Shutdown(SocketShutdown.Both);
            socket.Close();
        }

        public void sendDataToServer(string message) // метод отправки данных на сервер
        {
            byte[] data = Encoding.Unicode.GetBytes(message);
            socket.Send(data);
        }

        public void recieveDataFromServer() // метод получения данных от сервера
        {
            try
            {
                while (true)
                {
                    byte[] data = new byte[256]; // буфер для ответа
                    StringBuilder builder = new StringBuilder();
                    int bytes = 0; // количество полученных байт
                    do
                    {
                        bytes = socket.Receive(data, data.Length, 0);
                        builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                    }
                    while (socket.Available > 0);
                    if (builder.ToString() == "exit")
                    {
                        Console.WriteLine("\nСобеседник отключился!");
                        return;
                    }
                    else Console.WriteLine($"\nFrom server : {builder.ToString()}"); // все аналогично серверу
                }
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine($"Возникла проблема.\nИнформация о ней: {e.Message}");
            }
        }

    }
    

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Чат на двоих, введите 1/2 для выбора себя сервером/хостом");
            string input = Console.ReadLine(); // выбор меню
            if (input == "1")
            {
                Console.WriteLine("Введите IP и порт своего устройства через ':', \nили оставьте пустую строку для выбора значения из списка");
                input = Console.ReadLine();
                IPAddress addr = IPAddress.Any;
                int port = 0;
                if (input == "") // если хотят получить по умолчанию
                {
                    List<IPAddress> ip = Dns.GetHostEntry(Dns.GetHostName()).AddressList.ToList(); // получаем все IP на ПК
                    ip.RemoveAll((IPAddress pred_ip) => { return (pred_ip.ToString().Split('.').Length != 4); }); // убираем IPv6
                    for (int i = 0; i < ip.Count; i++)
                    {
                        Console.WriteLine($"{i+1}) {ip[i]}");
                    }
                    Console.Write("Выберите IP адрес : ");
                    addr = ip[Convert.ToInt32(Console.ReadLine()) - 1]; // выбираем из массива нужный
                    port = 10000 + (new Random()).Next(40000);
                }
                else
                {
                    try
                    {
                        addr = IPAddress.Parse(input.Split(':')[0]);
                        port = Convert.ToInt32(input.Split(':')[1]);
                    }
                    catch(Exception e)
                    {
                        Console.WriteLine("Введен неправильный формат данных!");
                        return;
                    }
                }
                try
                {
                    Console.WriteLine($"Ваши IP и порт - {addr.ToString()}:{port}. Сообщите эти данные собеседнику.\nДля окончания работы чата, напишите 'exit'.\n");
                    Server serv = new Server(addr, port); // создаем объект сервера
                    serv.startListening();
                    Thread thread = new Thread(() => serv.readDataFromClient()); // в отдельном потоке ожидаем сооьбщения от клиента
                    thread.Start();
                    while (true) // в основном потоке предлагаем пользователю отправить клиенту сообщение
                    {
                        if (!thread.IsAlive) // если поток чтения выключен, значит пришло от собеседника exit -> выходим
                            break;
                        Console.Write("Введите сообщение:");
                        string message = Console.ReadLine();
                        serv.sendDataToClient(message);
                        if (message == "exit")
                            break;
                    }
                    if (thread.IsAlive)
                        thread.Abort(); // выключаем поток, тк закончили работу
                    serv.disconnect();
                }
                catch (Exception e)
                {
                    Console.Clear();
                    Console.WriteLine($"Возникла проблема.\nИнформация о ней: {e.Message}");
                }
            }
            else if (input == "2")
            {
                Console.WriteLine("Введите IP и порт которые вам сообщит собеседник через ':'.");
                input = Console.ReadLine();
                IPAddress addr = IPAddress.Any;
                int port = 0;
                try
                {
                    addr = IPAddress.Parse(input.Split(':')[0]);
                    port = Convert.ToInt32(input.Split(':')[1]);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Введен неправильный формат данных!");
                    return;
                }
                try
                {
                    Console.WriteLine("Для окончания работы чата, напишите 'exit'.\n"); // все аналогично серверу
                    Client cli = new Client(addr, port);
                    Thread thread = new Thread(() => cli.recieveDataFromServer());
                    thread.Start();
                    while (true)
                    {
                        if (!thread.IsAlive)
                            break;
                        Console.Write("Введите сообщение:");
                        string message = Console.ReadLine();
                        cli.sendDataToServer(message);
                        if (message == "exit")
                            break;
                    }
                    if (thread.IsAlive)
                        thread.Abort();
                    cli.disconnect();
                }
                catch(Exception e)
                {
                    Console.Clear();
                    Console.WriteLine($"Возникла проблема.\nИнформация о ней: {e.Message}");
                }
            }
        }
    }
}
