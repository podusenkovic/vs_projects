#define NO_TRANSITION 9

#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <conio.h>
#include <sstream>
#include <iterator>

using namespace std;

vector<string> possible_outputs = { "G\tr","YL\tr","YL\tr","R\tg","R\tyl","R\tyl" };

// -----------------------to_sverofor_h----------------------
class svetofor_sm {
    int current_state;
    vector<vector<int>> states;

public:
    svetofor_sm(string file_name) : current_state(0) {
        ifstream input_file(file_name);
        string values;

        while (!input_file.eof()) {
            states.push_back(vector<int>());
            getline(input_file, values);
            istringstream string_stream(values);
            copy(istream_iterator<int>(string_stream), istream_iterator<int>(), back_inserter(states[states.size() - 1]));
        }

    }
    void reset() {
        current_state = 0;
    };

    void input(int value) {
        if (states[value][current_state] != NO_TRANSITION)
            current_state = states[value][current_state];
    };

    string get_result() {
        return possible_outputs[current_state];
    };
};
// ----------------------------------------------------------
//#include "Svetofor.h"

//------------------------------------------------------------------------------
int main()
{
    char cur_char;  //������� ��������� �� ����� ������
    string in_file;
    in_file = "input.txt";

    //���������� ������ svetofor_sm
    svetofor_sm svetofor("svetofor.txt");
    //����� 
    svetofor.reset();


    //��������� �������� ����� ��������� � ������� ������
    ifstream input_file(in_file.c_str());
    if (!input_file) {
        cout << "\nError reading \"input.txt\". No such file!";
        _getch();
        return -1;
    }

    int  K = 0;
    int  D = 0;
    int  Z = 0;
    int  Kcount = 0;
    int  Dcount = 0;
    int  Zcount = 0;
    string blank("        ");
    string print("        ");

    //����������� ������ ����
    while (!input_file.eof()) {

        input_file.get(cur_char);
        //-----------------------------------------------------------------------------
        if (cur_char == '4' || cur_char == '6') 
            K = 1;
        if (K == 1 && Kcount < 6) {
            if (cur_char == '2' || cur_char == '3' || cur_char == '0') {
                print[4] = 'K';
            }
            Kcount++;
        }
        if (Kcount == 6) {
            K = 0;
            Kcount = 0;
        }
        //-----------------------------------------------------------------------------
        if (cur_char == '2' || cur_char == '6') 
            D = 1;
        if (D == 1 && Dcount < 4) {
            if (cur_char == '1' || cur_char == '3' || cur_char == '0') {
                print[5] = 'D';
            }
            Dcount++;
        }
        if (Dcount == 4) {
            D = 0;
            Dcount = 0;
        }
        //----------------------------------------------------------------------------
        svetofor.input(K * 4 + D * 2 + Z);
        string y = svetofor.get_result();
        if (y == "YL\tr" || y == "R\tyl") 
            Z = 1;
        if (Z == 1 && Zcount < 2) {
            Zcount++;
        }
        if (Zcount == 2) {
            Z = 0;
            Zcount = 0;
        }
        //-----------------------------------------------------------------------------
        if (cur_char != '\n') {
            print[0] = cur_char;
            print += y;
            cout << print;
            print = blank;
        }  cout << '\n';
    }

    return 0;
}
