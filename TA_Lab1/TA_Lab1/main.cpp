//#define DEBUG_OUTPUT
#define USE_FILES

#include <iostream>
#include <string>
#include <math.h>
#include <vector>
#include <algorithm>
#include <fstream>

typedef unsigned int u32;

using namespace std;

int NUMBER_OF_BITS = 4;
const double EPS = 1E-4;
const string params = "xyzunmkpsetb";

class Element {
    u32 N;
    u32 I;
    u32 P = 0;
    bool used = false;

public:
    Element(u32 _N, u32 _I, u32 _P, bool _used = false) :
        N(_N), I(_I), P(_P), used(_used) {};

    void print_info() {
        printf("N = %2d, I = %1d, P = %1d\n", N, I, P);
    }

    u32 getN() { return N; }
    u32 getI() { return I; }
    u32 getP() { return P; }

    bool is_used() { return used; }
    void set_used() { used = true; }

    string get_result_vars(u32 bit_depth) {
        string result = "";
        for (u32 i = 0; i < bit_depth; i++) {
            if (P % 2 == 0)
                result = ((N % 2 == 0) ? "0" : "1") + result;
            else result = "-" + result;
            P /= 2;
            N /= 2;
        }
        return result;
    }
};

u32 count(u32 val) {
    u32 k = 0;
    while (val != 0) {
        k += val % 2;
        val /= 2;
    }
    return k;
}

int main()
{
    //string values = "11011101--101111"; // pasha
    //string values = "1111101-11-10010";
    //string values = "11011101--101100"; // my
    //string values = "11011101--100100"; // roma
    /* all arrays above got by using this program
            #include <iostream>
            #include <string>
            using namespace std;
            int main()
            {
                int values[] = {VALUES FROM TABLES};
                string str = "";
                for (int i = 0; i < 32; i++)
                    str.push_back('0');
                for (int i = 0; i < sizeof(values) / sizeof(int); i++)
                    str[values[31 - i]] = '1';
                cout << str << endl;
            }
    */
    ofstream temp("output.txt");
    temp.close();
    string values;
    vector<string> multivals;

#ifdef USE_FILES
    ifstream input_file("input.txt");
    while (!input_file.eof()) {
        input_file >> values;
        multivals.push_back(values);
    }
#endif
    for (int t = 0; t < multivals.size(); t++) {
        vector<vector<Element>> arrays;
        arrays.push_back(vector<Element>());
        values = multivals[t];
        int len = values.length();

        if (abs(log2(len) - int(log2(len))) > EPS)
            return -1;
        else NUMBER_OF_BITS = int(log2(len));

        for (u32 i = 0; i < pow(2, NUMBER_OF_BITS); i++)
            if (values[i] == '1')
                arrays[0].push_back(Element(i, count(i), 0));
            else if (values[i] == '-')
                arrays[0].push_back(Element(i, count(i), 0));

        vector<Element> start_array(arrays[0]);

        cout << "Array #0 : \n";
#ifdef DEBUG_OUTPUT
        for_each(arrays[0].begin(), arrays[0].end(), [](Element a) { a.print_info(); });
#endif

        int count_not_used = 0;
        int k = 0;
        while (1) {
            count_not_used = 0;
            arrays.push_back(vector<Element>());
            for (u32 i = 0; i < arrays[k].size(); i++) {
                for (u32 j = i; j < arrays[k].size(); j++) {
                    if (i == j)
                        continue;

                    if (arrays[k][j].getN() > arrays[k][i].getN() &&
                        arrays[k][j].getI() - arrays[k][i].getI() == 1 &&
                        count(arrays[k][j].getN() - arrays[k][i].getN()) == 1 &&
                        arrays[k][j].getP() == arrays[k][i].getP())
                    {
                        arrays[k + 1].push_back(Element(arrays[k][i].getN(),
                            arrays[k][i].getI(),
                            arrays[k][i].getP() + (arrays[k][j].getN() - arrays[k][i].getN())));
                        arrays[k][j].set_used();
                        arrays[k][i].set_used();
                    }
                }
                if (!arrays[k][i].is_used()) {
                    arrays[k + 1].push_back(Element(arrays[k][i].getN(), arrays[k][i].getI(), arrays[k][i].getP()));
                    count_not_used++;
                }
            }

            //printf("Array #%d : \n", k + 1);
            //for_each(arrays[k + 1].begin(), arrays[k + 1].end(), [](Element a) { a.print_info(); });

            if (count_not_used == arrays[k].size())
                break;

            arrays.erase(arrays.begin() + k);
        }
        arrays[k + 1].clear();
        arrays.push_back(vector<Element>());
#ifdef DEBUG_OUTPUT
        cout << "END OF THE WHILE\n";
#endif
        bool already_have = false;
        for (u32 i = 0; i < arrays[k].size(); i++) {
            for (u32 j = 0; j < arrays[k + 1].size(); j++) {
                if (arrays[k][i].getN() == arrays[k + 1][j].getN() &&
                    arrays[k][i].getI() == arrays[k + 1][j].getI() &&
                    arrays[k][i].getP() == arrays[k + 1][j].getP()) {
                    already_have = true;
                    break;
                }
            }
            if (!already_have) {
                arrays[k + 1].push_back(Element(arrays[k][i].getN(), arrays[k][i].getI(), arrays[k][i].getP()));
            }
            already_have = false;
        }

        int last = k + 1;
#ifdef DEBUG_OUTPUT
        cout << "Last array : \n";
        for_each(arrays[last].begin(), arrays[last].end(), [](Element a) { a.print_info(); });
#endif
        /*// useless, because we need to sort our table first
        cout << params.substr(0, NUMBER_OF_BITS) << endl;// "xyzu\n";
        for_each(arrays[last].begin(), arrays[last].end(), [](Element a) {
            cout << a.get_result_vars(NUMBER_OF_BITS) << endl;
            });
        */
        vector<vector<int>> table_for_mdnf;
        for (u32 i = 0; i < arrays[last].size(); i++) {
            table_for_mdnf.push_back(vector<int>());
            for (u32 j = 0; j < start_array.size(); j++)
                if ((start_array[j].getN() & ~arrays[last][i].getP()) == arrays[last][i].getN())
                    table_for_mdnf[i].push_back(1);
                else table_for_mdnf[i].push_back(0);
        }
#ifdef DEBUG_OUTPUT
        cout << "table with + and - BEFORE SORT\n";
        for_each(table_for_mdnf.begin(), table_for_mdnf.end(), [](vector<int> arr) {
            for_each(arr.begin(), arr.end(), [](int elem) { printf("%c", elem ? '+' : '-'); });
            printf("\n");
    });
#endif

        // sort to delete the longest implicant
        for (u32 i = 0; i < arrays[last].size() - 1; i++) {
            for (u32 j = i; j < arrays[last].size(); j++) {
                u32 count_i = 0, count_j = 0;
                for (u32 k = 0; k < table_for_mdnf[i].size(); k++) {
                    if (table_for_mdnf[i][k] == 1 && values[k] != '-')
                        count_i++;
                    if (table_for_mdnf[j][k] == 1 && values[k] != '-')
                        count_j++;
                }
                //if (count_if(table_for_mdnf[i].begin(), table_for_mdnf[i].end(), [](int val)) > count(table_for_mdnf[j].begin(), table_for_mdnf[j].end(), 1)) {
                if (count_i > count_j) {
                    swap(arrays[last][i], arrays[last][j]);
                    swap(table_for_mdnf[i], table_for_mdnf[j]);
                }
            }
        }
#ifdef DEBUG_OUTPUT
        cout << "Last array AFTER SORT: \n";
        for_each(arrays[last].begin(), arrays[last].end(), [](Element a) { a.print_info(); });

        cout << "table with + and - AFTER SORT\n";
        for_each(table_for_mdnf.begin(), table_for_mdnf.end(), [](vector<int> arr) {
            for_each(arr.begin(), arr.end(), [](int elem) { printf("%c", elem ? '+' : '-'); });
            printf("\n");
            });
#endif
        int pluses_in_cols = 0, good_cols = 0, flag = 0;
        for (u32 i = 0; i < arrays[last].size(); i++) {
            good_cols = 0;
            flag = 1;
            for (u32 j = 0; j < start_array.size(); j++) {
                pluses_in_cols = 0;
                for (u32 k = 0; k < arrays[last].size(); k++) {
                    if (i == k) continue;
                    pluses_in_cols += table_for_mdnf[k][j];
                }
                if (pluses_in_cols == 0 && (values[start_array[j].getN()] != '-'))
                    break;
                else good_cols++;
            }
            if (good_cols == start_array.size()) {
                arrays[last].erase(arrays[last].begin() + i);
                table_for_mdnf.erase(table_for_mdnf.begin() + i);
                i--;
            }
        }
#ifdef DEBUG_OUTPUT
        cout << "VERY Last array : \n";
        for_each(arrays[last].begin(), arrays[last].end(), [](Element a) { a.print_info(); });
#endif
        cout << params.substr(0, NUMBER_OF_BITS) << endl;
        for_each(arrays[last].begin(), arrays[last].end(), [](Element a) {
            cout << a.get_result_vars(NUMBER_OF_BITS) << endl;
            });
#ifdef USE_FILES
        ofstream output_file("output.txt", ofstream::app);
        output_file << params.substr(0, NUMBER_OF_BITS) << endl;
        for_each(arrays[last].begin(), arrays[last].end(), [&output_file](Element a) {
            output_file << a.get_result_vars(NUMBER_OF_BITS) << endl;
            });
        output_file.close();
#endif
    }
}
