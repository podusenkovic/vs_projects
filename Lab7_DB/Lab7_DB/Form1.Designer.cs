﻿namespace Lab7_DB
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridMain = new System.Windows.Forms.DataGridView();
            this.comboBoxChooseTable = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button_submit = new System.Windows.Forms.Button();
            this.button_undoall = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxChooseQuery = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridMain)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridMain
            // 
            this.dataGridMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridMain.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridMain.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridMain.Location = new System.Drawing.Point(12, 31);
            this.dataGridMain.Name = "dataGridMain";
            this.dataGridMain.Size = new System.Drawing.Size(776, 371);
            this.dataGridMain.TabIndex = 0;
            this.dataGridMain.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridMain_DataError);
            // 
            // comboBoxChooseTable
            // 
            this.comboBoxChooseTable.Enabled = false;
            this.comboBoxChooseTable.FormattingEnabled = true;
            this.comboBoxChooseTable.Location = new System.Drawing.Point(72, 4);
            this.comboBoxChooseTable.Name = "comboBoxChooseTable";
            this.comboBoxChooseTable.Size = new System.Drawing.Size(200, 21);
            this.comboBoxChooseTable.TabIndex = 1;
            this.comboBoxChooseTable.SelectedIndexChanged += new System.EventHandler(this.comboBoxChooseTable_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Tables:";
            // 
            // button_submit
            // 
            this.button_submit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_submit.Enabled = false;
            this.button_submit.Location = new System.Drawing.Point(682, 409);
            this.button_submit.Name = "button_submit";
            this.button_submit.Size = new System.Drawing.Size(106, 29);
            this.button_submit.TabIndex = 3;
            this.button_submit.Text = "Submit changes";
            this.button_submit.UseVisualStyleBackColor = true;
            this.button_submit.Click += new System.EventHandler(this.button_submit_Click);
            // 
            // button_undoall
            // 
            this.button_undoall.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_undoall.Enabled = false;
            this.button_undoall.Location = new System.Drawing.Point(566, 409);
            this.button_undoall.Name = "button_undoall";
            this.button_undoall.Size = new System.Drawing.Size(110, 29);
            this.button_undoall.TabIndex = 4;
            this.button_undoall.Text = "Undo all changes";
            this.button_undoall.UseVisualStyleBackColor = true;
            this.button_undoall.Click += new System.EventHandler(this.button_undoall_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(734, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 16);
            this.label2.TabIndex = 6;
            this.label2.Text = "Queries";
            // 
            // comboBoxChooseQuery
            // 
            this.comboBoxChooseQuery.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxChooseQuery.Enabled = false;
            this.comboBoxChooseQuery.FormattingEnabled = true;
            this.comboBoxChooseQuery.Location = new System.Drawing.Point(527, 3);
            this.comboBoxChooseQuery.Name = "comboBoxChooseQuery";
            this.comboBoxChooseQuery.Size = new System.Drawing.Size(200, 21);
            this.comboBoxChooseQuery.TabIndex = 5;
            this.comboBoxChooseQuery.SelectedIndexChanged += new System.EventHandler(this.comboBoxChooseQuery_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 409);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(110, 29);
            this.button1.TabIndex = 7;
            this.button1.Text = "Login";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBoxChooseQuery);
            this.Controls.Add(this.button_undoall);
            this.Controls.Add(this.button_submit);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxChooseTable);
            this.Controls.Add(this.dataGridMain);
            this.Name = "Form1";
            this.Text = "Lab7";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridMain)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridMain;
        private System.Windows.Forms.ComboBox comboBoxChooseTable;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_submit;
        private System.Windows.Forms.Button button_undoall;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxChooseQuery;
        private System.Windows.Forms.Button button1;
    }
}

