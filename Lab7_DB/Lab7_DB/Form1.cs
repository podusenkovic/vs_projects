﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Sql;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Diagnostics;
using System.Globalization;
using System.Data.SqlClient;

namespace Lab7_DB
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");
        }

        private SqlConnection myConnection;
        private string chosenTable = "";

        private string connectionStringWithLogins = "";

        private int chosenQueryID = 0;

        private Dictionary<string, List<KeyValuePair<string, Type>>> procsParametersNames = new Dictionary<string, List<KeyValuePair<string, Type>>>();

        private List<string> queriesArray = new List<string>();

        private List<bool> rowsThatNeedToAddInDB = new List<bool>();

        private Type getTypeFromItsName(string name)
        {
            switch (name)
            {
                case "date": return typeof(DateTime);
                case "float": return typeof(float);
                case "int": return typeof(int);
                case "varchar": return typeof(string);
                case "nchar": return typeof(string);
                case "money": return typeof(int);
                default: return typeof(string);
            }
        }

        // Загрузка всех доступных данному пользователю функций, процедур и представлений
        private void loadQueries()
        {
            string command = "use DB_TechPassports;\n" +
                "SELECT SPECIFIC_NAME\n" +
                "FROM INFORMATION_SCHEMA.ROUTINES\n" +
                "WHERE SUBSTRING(SPECIFIC_NAME, 0, 3) != 'sp' AND SUBSTRING(SPECIFIC_NAME, 0, 3) != 'fn'";
            SqlDataReader rd = execCommand(command, new List<SqlParameter>());
            while (rd.Read())
            {
                queriesArray.Add(rd.GetValue(0).ToString());
                command = "use DB_TechPassports;\n " +
                    "SELECT PARAMETER_NAME, DATA_TYPE\n " +
                    "FROM INFORMATION_SCHEMA.PARAMETERS\n " +
                    $"WHERE SPECIFIC_NAME = '{rd.GetValue(0).ToString()}' AND PARAMETER_MODE = 'IN'";
                SqlDataReader rd2 = execCommand(command, new List<SqlParameter>());
                while (rd2.Read())
                {
                    if (procsParametersNames.Keys.Contains(rd.GetString(0)))
                    {
                        procsParametersNames[rd.GetString(0)].Add(new KeyValuePair<string, Type>(rd2.GetString(0), getTypeFromItsName(rd2.GetString(1))));
                    }
                    else
                    {
                        procsParametersNames.Add(rd.GetString(0), new List<KeyValuePair<string, Type>>());
                        procsParametersNames[rd.GetString(0)].Add(new KeyValuePair<string, Type>(rd2.GetString(0), getTypeFromItsName(rd2.GetString(1))));
                    }
                }

            }
            for (int i = 0; i < queriesArray.Count; i++)
                comboBoxChooseQuery.Items.Add(queriesArray[i]);
        }

        private void connectAndOpenDB(string connectionString)
        {
            if (myConnection != null && myConnection.State == ConnectionState.Open)
                myConnection.Close();
            myConnection = new SqlConnection(connectionString);
            myConnection.Open();
        }
        
        // Кнопка логина
        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (myConnection != null)
                    if (myConnection.State != ConnectionState.Closed)
                        myConnection.Close();
                dataGridMain.Rows.Clear();
                dataGridMain.Columns.Clear();
                comboBoxChooseTable.Enabled = false;
                comboBoxChooseQuery.Enabled = false;
                button_submit.Enabled = false;
                button_undoall.Enabled = false;
                comboBoxChooseQuery.Items.Clear();
                comboBoxChooseTable.Items.Clear();
                procsParametersNames = new Dictionary<string, List<KeyValuePair<string, Type>>>();
                queriesArray.Clear();
                string login = Prompt.ShowDialog("Input login", "Loginform");
                if (login == "qw" || login == "")
                {
                    connectionStringWithLogins = Properties.Settings.Default.DBConnectionString + ";Integrated Security=True;";
                    comboBoxChooseTable.Enabled = true;
                    button_submit.Enabled = true;
                    button_undoall.Enabled = true;
                }
                else
                {
                    string password = Prompt.ShowDialog("Input password", "Loginform");
                    connectionStringWithLogins = Properties.Settings.Default.DBConnectionString + $";User ID={login};Password={password}";
                }
                connectAndOpenDB(connectionStringWithLogins);
                if (myConnection.State != ConnectionState.Open)
                {
                    MessageBox.Show("DB couldn't be opened, sorry!", "Warning!");
                    Close();
                }
                addTablesToComboBox();
                loadQueries();

                comboBoxChooseQuery.Enabled = true;
                comboBoxChooseTable.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\nИзвините за предоставленные неудобства!", "Warning!");
            }
        }


        private void addTablesToComboBox()
        {
            var schema = myConnection.GetSchema("Tables");
            foreach (DataRow row in schema.Rows)
            {
                //Debug.WriteLine(row["TABLE_TYPE"]);
                if ((row["TABLE_TYPE"].ToString() == "BASE TABLE") && (row["TABLE_NAME"].ToString() != "sysdiagrams"))
                    comboBoxChooseTable.Items.Add($"{row["TABLE_NAME"]}");
            }
        }

        private SqlDataReader execCommand(string command, List<SqlParameter> queryParams)
        {
            SqlCommand cmd = new SqlCommand
            {
                Connection = myConnection,
                CommandText = command
            };
            foreach (var i in queryParams)
            {
                cmd.Parameters.Add(i);
            }
            return cmd.ExecuteReader();
        }
        private void execCommandWithoutReturn(string command, List<SqlParameter> queryParams)
        {
            SqlCommand cmd = new SqlCommand
            {
                Connection = myConnection,
                CommandText = command
            };
            foreach (var i in queryParams)
            {
                cmd.Parameters.Add(i);
            }
            cmd.ExecuteNonQuery();
            Debug.WriteLine("kekw4inside");
        }

        // получение индекса праймари кей
        private int getPrimaryColumn(string table) // TODO RETURN MANY PRIMARY COLS
        {
            int cnt = 0;
            string command = "use DB_TechPassports;\n" +
                "SELECT ORDINAL_POSITION\n " +
                "FROM INFORMATION_SCHEMA.COLUMNS\n " +
                $"WHERE TABLE_NAME = '{table}' AND COLUMN_NAME = ANY(\n" +
                "SELECT COLUMN_NAME\n" +
                "FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE\n" +
                $"WHERE TABLE_NAME = '{table}' AND SUBSTRING(CONSTRAINT_NAME, 0, 3) = 'PK');";
            SqlDataReader rd = execCommand(command, new List<SqlParameter>());
            rd.Read();
            cnt = rd.GetInt32(0) - 1;
            Debug.WriteLine(cnt);
            return cnt;
        }

        // генерация значений
        private string createValuesFromCell(DataGridViewCell cell)
        {
            Debug.WriteLine(cell.ValueType);
            string new_value = "";
            string additionals = "";
            if (cell.ValueType == typeof(string))
            {
                additionals = "'";
                new_value = (string)cell.Value;
                Debug.WriteLine("CHECK_STRING");
            }
            else if (cell.ValueType == typeof(DateTime))
            {
                additionals = "'";
                new_value = $"{((DateTime)cell.Value).Month}/{((DateTime)cell.Value).Day}/{((DateTime)cell.Value).Year}";//((DateTime)cell.Value).ToShortDateString().Replace('.', '/');
                Debug.WriteLine("CHECK_DATETIME");
            }
            else if (cell.ValueType == typeof(int))
            {
                additionals = "";
                new_value = ((int)cell.Value).ToString();
                Debug.WriteLine("CHECK_INT");
            }
            else if (cell.ValueType == typeof(decimal))
            {
                additionals = "";
                new_value = ((decimal)cell.Value).ToString().Replace(',', '.');
                Debug.WriteLine("CHECK_DECIMAL");
            }
            else if (cell.ValueType == typeof(bool))
            {
                additionals = "";
                if (cell.Value == null || cell.Value.Equals(false))
                    cell.Value = false;
                new_value = (cell.Value.Equals(true) ? "TRUE" : "FALSE");
                Debug.WriteLine("CHECK_BOOL");
            }
            else if (cell.ValueType == typeof(double) || cell.ValueType == typeof(float))
            {
                additionals = "'";
                new_value = ((double)cell.Value).ToString();
                Debug.WriteLine("CHECK_DBL_FLT");
            }
            return $"{additionals}{new_value}{additionals}";
        }

        private string generateUpdateCommand(string table, int row_in_grid, ref List<SqlParameter> query_params)
        {
            query_params = new List<SqlParameter>();
            string to_return = $"UPDATE [{table}] " + "SET ";
            for (int j = 1; j < dataGridMain.Columns.Count; j++)
            {
                Debug.WriteLine(dataGridMain.Columns[j].HeaderText);
                to_return += $"[{dataGridMain.Columns[j].HeaderText}]= " + "@" + dataGridMain.Columns[j].HeaderText.ToUpperInvariant().Replace(' ', '_') + ((j == dataGridMain.Columns.Count - 1) ? " " : ",") + "\n";
                query_params.Add(new SqlParameter(dataGridMain.Columns[j].HeaderText.ToUpperInvariant().Replace(' ', '_'), createValuesFromCell(dataGridMain[j, row_in_grid])));
            }
            to_return += $"WHERE [{dataGridMain.Columns[getPrimaryColumn(table)].HeaderText}]={createValuesFromCell(dataGridMain[getPrimaryColumn(table), row_in_grid])}";
            //---------------------------
            Debug.WriteLine(to_return);
            //---------------------------
            return to_return;
        }

        private string generateDeleteCommand(string table, string primary_key_to_delete)
        {
            Debug.WriteLine(primary_key_to_delete);
            string to_return = $"DELETE FROM [{table}] " +
                               $"WHERE [{dataGridMain.Columns[getPrimaryColumn(table)].HeaderText}]=" +
                               $"{primary_key_to_delete}";
            //---------------------------
            Debug.WriteLine(to_return);
            //---------------------------
            return to_return;
        }

        private string generateInsertCommand(string table, int row_in_grid, ref List<SqlParameter> queryParams)
        {
            string to_return = $"INSERT INTO [{table}] " + "VALUES (";
            for (int j = 0; j < dataGridMain.Columns.Count; j++)
            {
                queryParams.Add(new SqlParameter(((char)(j + 'A')).ToString(), createValuesFromCell(dataGridMain[j, row_in_grid])));
                to_return += $"@{(char)(j + 'A')}" + ((j == dataGridMain.Columns.Count - 1) ? " " : ",");
            }
            to_return += ")";
            //---------------------------
            Debug.WriteLine(to_return);
            //---------------------------
            return to_return;
        }

        // основная перезагрузка центральной таблицы
        private void reloadShownThing(int what_to_show) // 1 for table, 2 for query
        {
            try
            {
                string command_to_do = "";
                List<SqlParameter> queryParams = new List<SqlParameter>();
                if (what_to_show == 1)
                    command_to_do = $"SELECT * FROM {chosenTable}";
                else if (what_to_show == 2)
                {
                    string queryParameters = "";
                    for (int i = 0; i < procsParametersNames[queriesArray[chosenQueryID]].Count; i++)
                    {
                        string inputValue = Prompt.ShowDialog($"Input {procsParametersNames[queriesArray[chosenQueryID]][i].Key}\n" +
                            $"It has type is {procsParametersNames[queriesArray[chosenQueryID]][i].Value.Name}", "Input window");
                        if (inputValue == "")
                        {
                            MessageBox.Show("Abort operation, you must fill the form", "Abort!");
                            return;
                        }
                        else
                        {
                            queryParameters += procsParametersNames[queriesArray[chosenQueryID]][i].Key + " " + ((i == procsParametersNames[queriesArray[chosenQueryID]].Count - 1) ? " " : ", ");
                            queryParams.Add(new SqlParameter(procsParametersNames[queriesArray[chosenQueryID]][i].Key, inputValue));
                        }
                    }
                    Debug.WriteLine(queryParameters);
                    command_to_do = $"EXEC [{queriesArray[chosenQueryID]}] {queryParameters}";//$"EXEC [{queriesArray[chosenQueryID]}]";
                }
                SqlDataReader rd = execCommand(command_to_do, queryParams);
                if (rd.IsClosed)
                    return;
                dataGridMain.Rows.Clear();
                dataGridMain.Columns.Clear();
                for (int i = 0; i < rd.FieldCount; i++)
                {
                    if (rd.GetFieldType(i) != typeof(bool))
                    {
                        dataGridMain.Columns.Add(rd.GetName(i).Replace(' ', '_'), rd.GetName(i));
                        dataGridMain.Columns[dataGridMain.Columns.Count - 1].ValueType = rd.GetFieldType(i);
                    }
                    else
                    {
                        DataGridViewCheckBoxColumn col = new DataGridViewCheckBoxColumn();
                        col.Name = rd.GetName(i).Replace(' ', '_');
                        col.HeaderText = rd.GetName(i);
                        col.ThreeState = false;
                        dataGridMain.Columns.Add(col);
                    }
                }
                while (rd.Read())
                {
                    DataGridViewRow rowToAdd = new DataGridViewRow();
                    rowToAdd.CreateCells(dataGridMain);
                    for (int i = 0; i < rd.FieldCount; i++)
                        rowToAdd.Cells[i].Value = ((rd.GetFieldType(i) != typeof(bool)) ? rd.GetValue(i) : rd.GetValue(i).ToString().ToUpper());
                    dataGridMain.Rows.Insert(dataGridMain.Rows.Count - 1, rowToAdd);
                }
                rd.Close();
                Debug.WriteLine("INFO ABOUT RD IS HERE : " + rd.IsClosed);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\nИзвините за предоставленные неудобства!", "Warning!");
            }
        }


        private void comboBoxChooseTable_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridMain.Enabled = true;
            if (!button_submit.Visible)
                button_submit.Visible = true;
            if (!button_undoall.Visible)
                button_undoall.Visible = true;
            chosenTable = (string)comboBoxChooseTable.SelectedItem;
            reloadShownThing(1);
        }
        private void button_undoall_Click(object sender, EventArgs e)
        {
            reloadShownThing(1);
        }

        private string checkLineAndReturnCmd(SqlDataReader reader, ref List<SqlParameter> queryParams)
        {
            //if (!reader.Read())
            //    return;
            int primary_col = getPrimaryColumn(chosenTable);
            bool found_row_in_grid = false;
            for (int i = 0; i < dataGridMain.RowCount - 1; i++)
            {
                if (reader.GetValue(primary_col).ToString() == dataGridMain[primary_col, i].Value.ToString())
                {
                    rowsThatNeedToAddInDB[i] = false;
                    found_row_in_grid = true;
                    for (int j = 0; j < dataGridMain.ColumnCount; j++)
                    {
                        if (reader.GetValue(j).ToString().ToUpper() != dataGridMain[j, i].Value.ToString().ToUpper())
                        {
                            return generateUpdateCommand(chosenTable, i, ref queryParams);//need_update = true;
                        }
                    }
                }
            }
            if (!found_row_in_grid)
                return generateDeleteCommand(chosenTable, reader.GetValue(primary_col).ToString()); //need_delete = true;
            else return "NONE"; // everything is ok!
        }

        private void button_submit_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < dataGridMain.RowCount - 1; i++)
                    rowsThatNeedToAddInDB.Add(true);

                SqlConnection.ClearAllPools();
                SqlDataReader rd = execCommand($"SELECT * FROM {chosenTable}", new List<SqlParameter>());

                if (!rd.HasRows)
                    return;
                while (rd.Read())
                {
                    List<SqlParameter> queryParams = new List<SqlParameter>();
                    string result = checkLineAndReturnCmd(rd, ref queryParams);
                    if (result != "NONE")
                    {
                        Debug.WriteLine(result);
                        Debug.WriteLine("kekw1");
                        execCommandWithoutReturn(result, queryParams);
                        Debug.WriteLine("kekw2");
                    }
                }
                Debug.WriteLine(rowsThatNeedToAddInDB.Count);
                for (int i = 0; i < rowsThatNeedToAddInDB.Count; i++)
                    if (rowsThatNeedToAddInDB[i])
                    {
                        List<SqlParameter> queryParams = new List<SqlParameter>();
                        string result = generateInsertCommand(chosenTable, i, ref queryParams);
                        execCommandWithoutReturn(result, queryParams);
                    }
                rowsThatNeedToAddInDB.Clear();
                rd.Close();
                Debug.WriteLine("INFO ABOUT RD IS HERE : " + rd.IsClosed);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\nИзвините за предоставленные неудобства!", "Warning!");
                reloadShownThing(1);
            }
        }
        private void dataGridMain_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("Please, input values only the same type as in the column!\nThank you!", "Warning!");
        }

        // Вызывается при закрытии окна
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (myConnection != null && myConnection.State != ConnectionState.Closed)
                myConnection.Close();
        }

        private void comboBoxChooseQuery_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridMain.Enabled = false;
            if (button_submit.Visible)
                button_submit.Visible = false;
            if (button_undoall.Visible)
                button_undoall.Visible = false;
            chosenQueryID = comboBoxChooseQuery.SelectedIndex;
            reloadShownThing(2);
        }
    }

    // доп класс для диалоговых окошек ввода данных (параметры запросов, логин\пароль)
    public static class Prompt
    {
        public static string ShowDialog(string text, string caption)
        {
            Form prompt = new Form();
            prompt.Width = 400;
            prompt.Height = 400;
            prompt.Text = caption;
            Label textLabel = new Label() { Left = 30, Top = 10, Width = 350, Height = 180, Text = text, Font = new Font("Times new roman", 15) };
            TextBox inputBox = new TextBox() { Left = 30, Top = 200, Width = 300 };
            Button confirmation = new Button() { Text = "Ok", Left = 340, Width = 40, Top = 200 };
            confirmation.Click += (sender, e) => { prompt.Close(); };
            prompt.Controls.Add(confirmation);
            prompt.Controls.Add(textLabel);
            prompt.Controls.Add(inputBox);
            prompt.ShowDialog();
            return inputBox.Text;
        }
    }
}
