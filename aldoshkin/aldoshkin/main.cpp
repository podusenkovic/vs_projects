#include <iostream>
#include <string>

using namespace std;


class Massiv {
	int arr[10];

public:
	Massiv() {
		for (int i = 0; i < 10; i++)
			arr[i] = rand() % 10;
	}

	Massiv(int a) {
		for (int i = 0; i < 10; i++)
			arr[i] = a;
	}

	Massiv(const Massiv& a) {
		for (int i = 0; i < 10; i++)
			arr[i] = a.arr[i];
	}

	Massiv& operator+(const Massiv& a) {
		static Massiv *n = new Massiv(0);
		for (int i = 0; i < 10; i++)
			n->arr[i] = arr[i] + a.arr[i];
		return *n;
	}

	void operator-=(int a) {
		for (int i = 0; i < 10; i++)
			arr[i] -= a;
	}

	void print() {
		for (int i = 0; i < 10; i++)
			cout << arr[i] << " ";
		cout << endl;
	}
};

int main() {
	Massiv arr1(1), arr2, arr3;
	arr1.print();
	arr2.print();
	
	arr3 = arr1 + arr2;
	arr3.print();

	int a = 5;
	cout << a;
	a -= 3; // a = a - 3;
	cout << a;

	system("pause");
}