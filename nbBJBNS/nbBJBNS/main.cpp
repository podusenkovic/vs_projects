
#include <iostream>
#include <time.h>
#include <cstdlib>
#include <conio.h>

using namespace std;

int** createMatrix(int rows, int cols) {
	int **matrix = new int*[rows];
	for (int i = 0; i < cols; i++) matrix[i] = new int[rows];
	return matrix;
}

void formMatrix(int** matrix, int rows, int cols) {
	for (int i = 0; i < rows; i++)
		for (int j = 0; j < cols; j++)
			matrix[i][j] = rand() % 10;

}

void printMatrix(int** matrix, int rows, int cols) {
	cout << endl;
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
			cout << matrix[i][j] << ' ';

		cout << endl;
	}
}

int main() {

	srand(time(0));

	int rows, cols = 0;

	cout << "rows:: ";
	cin >> rows;

	cout << "cols:: ";
	cin >> cols;

	int** matrix00 = createMatrix(rows, cols);
	formMatrix(matrix00, rows, cols);

	cout << endl << "matrix:: ";
	printMatrix(matrix00, rows, cols);

	cout << endl;

	_getch();

	return 0;
}