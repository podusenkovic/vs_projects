﻿using System;
using System.IO;

namespace generateWalls
{
    class Program
    {
        static void Main(string[] args)
        {
            Random r = new Random();
            StreamWriter sw = new StreamWriter("coords.txt");
            for (int i = 0; i < 16; i++)
            {
                sw.Write(r.Next(64) * 10 + ", ");
            }
            sw.WriteLine();
            for (int i = 0; i < 16; i++)
            {
                sw.Write(r.Next(48) * 10 + ", "); 
            }
            sw.Close();
        }
    }
}
