#include <iostream>
#define d (double)

using namespace std;

bool Eq(double a, double b, double c) {
	int pog = 2;
	return abs(a - b) < pog && abs(a - c) < pog && abs(a - b) < pog;
}

double toDeg(double a, double full) {
	return floor(a / full * 360);
}

int main() {
	int count = 0;
	for (int h = 0; h < 24; h++)
		for (int m = 0; m < 60; m++)
			for (int s = 0; s < 60; s++) {

				double H = h%12 + m / d 60 + s / d 3600;
				double M = m + s / d 60;
				double S = s;
				int hdeg = toDeg(H, 12);
				int mdeg = toDeg(M, 60);
				int sdeg = toDeg(S, 60);
				
				if (Eq(hdeg, mdeg, sdeg)) {
					cout << h << ":" << m << ":" << s << endl;
					count++;
				}
			}
	cout << count << endl;
	system("pause");
}