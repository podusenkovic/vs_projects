﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace queries_parser_lab10
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader sr = new StreamReader(@"C:\Users\1\Documents\SQL Server Management Studio\Queries.txt", Encoding.Default);
            string all_text = sr.ReadToEnd();
            string[] queries = all_text.Split('|');
            for (int i = 1; i < queries.Count(); i++)
            {
                StreamWriter sw = new StreamWriter($@"C:\Users\1\Documents\SQL Server Management Studio\queries\{queries[i].Split(':')[0]}.sql", false, Encoding.Default);
                sw.WriteLine(queries[i].Split(':')[1]);
                sw.Close();
            }
            sr.Close();
        }
    }
}
