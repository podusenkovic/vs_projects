#define NO_TRANSITION 9

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <sstream>
#include <iterator>
#include <conio.h>
#include <stack>
#include <algorithm>

using namespace std;

string last_solved_name = "";
vector<string> names;
map<string, unsigned int> names_vals;

class Translator {
	vector<unsigned int> Lexems;
	vector<vector<int>> states;
	vector<vector<int>> results;
	int current_state;
public:
	Translator(string file_name_fs, string file_name_fy) : current_state(0) {
		ifstream input_file(file_name_fs);
		string values;

		while (!input_file.eof()) {
			states.push_back(vector<int>());
			getline(input_file, values);
			istringstream string_stream(values);
			copy(istream_iterator<int>(string_stream), istream_iterator<int>(), back_inserter(states[states.size() - 1]));
		}
		input_file.close();

		input_file.open(file_name_fy);

		while (!input_file.eof()) {
			results.push_back(vector<int>());
			getline(input_file, values);
			istringstream string_stream(values);
			copy(istream_iterator<int>(string_stream), istream_iterator<int>(), back_inserter(results[results.size() - 1]));
		}
		input_file.close();
	}

	void clearAll() {
		Lexems.clear();
		current_state = 0;
	}

	vector<unsigned int> getLexems() {
		return Lexems;
	}
	
	vector<string> getNames() {
		return names;
	}

	void printLexems() {
		cout << "LEXEMS:\n";
		for (size_t i = 0; i < Lexems.size(); i++)
			if ((Lexems[i] >> 30) == 0x0)
				printf_s("%d ", Lexems[i]);
			else if ((Lexems[i] >> 30) == 0x1)
				printf_s("0x%x ", Lexems[i]);
			else if ((Lexems[i] >> 30) == 0x2) {
				unsigned char temp = (Lexems[i] & 0xff);
				printf_s("%c ", temp);
			}
		cout << "\nNames\n";
		for (size_t i = 0; i < names.size(); i++)
			cout << names[i];
		cout << "\n";
	}
	
	void input(unsigned char symbol) {
		int int_symbol;
		static string name = "";
		static unsigned int value_int = 0;
		if ((symbol >= 'a' && symbol <= 'z') | (symbol >= 'A' && symbol <= 'Z'))
			int_symbol = 0;
		else if (symbol >= '0' && symbol <= '9')
			int_symbol = 1;
		else if (symbol == '=')
			int_symbol = 2;
		else if (symbol == '(')
			int_symbol = 3;
		else if (symbol == '+' || symbol == '-' || symbol == '*' || symbol == '/')
			int_symbol = 4;
		else if (symbol == ')')
			int_symbol = 5;
		else if (symbol == ';')
			int_symbol = 6;
		else int_symbol = NO_TRANSITION;

		switch (results[int_symbol][current_state]) {
		case 1: 
			name.push_back(symbol);
			break;
		case 2:
			if (find(names.begin(), names.end(), name) == names.end())
				names.push_back(name);
			last_solved_name = name;
			name = "";
			break;
		case 3:
			Lexems.push_back(0x2 << 30 | symbol);
			break;
		case 4:
			value_int = value_int * 10 + (symbol - '0');
			break;
		case 5:
			if (find(names.begin(), names.end(), name) == names.end())
				names.push_back(name);
			Lexems.push_back(0x1 << 30 | (find(names.begin(), names.end(), name) - names.begin()));
			name = "";
			Lexems.push_back(0x2 << 30 | symbol);
			break;
		case 6:
			if (find(names.begin(), names.end(), name) == names.end())
				names.push_back(name);
			Lexems.push_back(0x1 << 30 | (find(names.begin(), names.end(), name) - names.begin()));
			name = "";
			Lexems.push_back(0x2 << 30 | symbol);
			cout << "trans THE END\n";
			break;
		case 7:
			Lexems.push_back(0x0 << 30 | value_int);
			value_int = 0;
			Lexems.push_back(0x2 << 30 | symbol);
			break;
		case 8:
			Lexems.push_back(0x0 << 30 | value_int);
			value_int = 0;
			Lexems.push_back(0x2 << 30 | symbol);
			cout << "trans THE END\n";
			break;
		case 9:
			Lexems.push_back(0x2 << 30 | symbol);
			cout << "trans THE END\n";
			break;
		default:
			break;
		}

		if (states[int_symbol][current_state] != NO_TRANSITION)
			current_state = states[int_symbol][current_state];
	}
};


class CreatePoliz {
	int current_state;
	vector<unsigned int> poliz;
	vector<vector<int>> rangs;
	vector<vector<int>> states;
	vector<vector<int>> results;
	stack<unsigned int> magazine;
public:
	CreatePoliz(string file_name_rangs, string file_name_fs, string file_name_fy) : current_state(0){
		ifstream input_file(file_name_rangs);
		string values;

		while (!input_file.eof()) {
			rangs.push_back(vector<int>());
			getline(input_file, values);
			istringstream string_stream(values);
			copy(istream_iterator<int>(string_stream), istream_iterator<int>(), back_inserter(rangs[rangs.size() - 1]));
		}
		input_file.close();

		input_file.open(file_name_fs);

		while (!input_file.eof()) {
			states.push_back(vector<int>());
			getline(input_file, values);
			istringstream string_stream(values);
			copy(istream_iterator<int>(string_stream), istream_iterator<int>(), back_inserter(states[states.size() - 1]));
		}
		input_file.close();

		input_file.open(file_name_fy);

		while (!input_file.eof()) {
			results.push_back(vector<int>());
			getline(input_file, values);
			istringstream string_stream(values);
			copy(istream_iterator<int>(string_stream), istream_iterator<int>(), back_inserter(results[results.size() - 1]));
		}
		input_file.close();


		magazine.push((0x2 << 30) | '@');
	}

	void clearAll() {
		poliz.clear();
	}

	void printPoliz() {
		cout << "POLIZ:\n";
		for (size_t i = 0; i < poliz.size(); i++)
			if ((poliz[i] >> 30) == 0x0)
				printf_s("%d ", poliz[i]);
			else if ((poliz[i] >> 30) == 0x2)
				printf_s("%c ", poliz[i] & 0xff);
			else if ((poliz[i] >> 30) == 0x1)
				printf_s("var#%d ", poliz[i] & 0xff);
		cout << "\n";
	}

	vector<unsigned int> getPoliz() {
		return poliz;
	}

	int input(unsigned int value) {
		int flag = 0;
		int int_symbol = -1, int_mag_sym = -1;
		char symbol = value & 0xff;
		char mag_sym = magazine.top() & 0xff;
		if (symbol == '(')
			int_symbol = 0;
		else if (symbol == ')')
			int_symbol = 1;
		else if (symbol == '+' || symbol == '-')
			int_symbol = 2;
		else if (symbol == '*' || symbol == '/')
			int_symbol = 3;
		else if (symbol == ';')
			int_symbol = 4;
		else if (symbol == '@')
			int_symbol = 5;

		if (mag_sym == '(')
			int_mag_sym = 0;
		else if (mag_sym == ')')
			int_mag_sym = 1;
		else if (mag_sym == '+' || mag_sym == '-')
			int_mag_sym = 2;
		else if (mag_sym == '*' || mag_sym == '/')
			int_mag_sym = 3;
		else if (mag_sym == ';')
			int_mag_sym = 4;
		else if (mag_sym == '@')
			int_mag_sym = 5;
		
		int x = 0;
		if ((((value >> 30)) & 3) == 0x0 || ((value >> 30) & 3) == 0x1)
			x = 1;
		else if (((value >> 30) & 3) == 0x2 && 
			((value & 0xff) != ')') && 
			((value & 0xff) != ';') &&
			rangs[0][int_symbol] > rangs[1][int_mag_sym])
			x = 2;
		else if (((value >> 30) & 3) == 0x2 &&
			rangs[0][int_symbol] <= rangs[1][int_mag_sym])
			x = 3;
		else if (((value >> 30) & 3) == 0x2 &&
			((value & 0xff) == ')') &&
			rangs[0][int_symbol] > rangs[1][int_mag_sym])
			x = 4;
		else if (((value >> 30) & 3) == 0x2 &&
			((value & 0xff) == ';') &&
			rangs[0][int_symbol] > rangs[1][int_mag_sym])
			x = 5;

		switch (results[x - 1][current_state]) {
		case 1:
			poliz.push_back(value);
			break;
		case 2:
			magazine.push(value);
			break;
		case 3:
			poliz.push_back(magazine.top());
			magazine.pop();
			flag = 1;
			break;
		case 4:
			magazine.pop();
			break;
		case 5:
			poliz.push_back(value);
			cout << "poliz the end!\n";
			break;
		default:
			break;
		}

		if (states[x - 1][current_state] != NO_TRANSITION)
			current_state = states[x - 1][current_state];
		
		return flag;
	}
};

class SolvePoliz {
	stack<unsigned int> magazine;
	unsigned int final_result;
public:
	SolvePoliz() {
		final_result = 0;
		magazine.push(0x2 << 30 | '@');
	}

	void clearAll() {
		final_result = 0;
		while (magazine.top() != (0x2 << 30 | '@'))
			magazine.pop();
	}

	void input(unsigned int value) {
		if (((value >> 30) & 3) == 0x0)
			magazine.push(value);
		else if (((value >> 30) & 3) == 0x2 && (value & 0xff) != ';') {
			unsigned int b = magazine.top();
			magazine.pop();
			unsigned int a = magazine.top();
			magazine.pop();
			unsigned int result;
			if ((value & 0xff) == '+')
				result = a + b;
			else if ((value & 0xff) == '-')
				result = a - b;
			else if ((value & 0xff) == '*')
				result = a * b;
			else if ((value & 0xff) == '/')
				result = a / b;
			magazine.push(result);
		}
		else if (((value >> 30) & 3) == 0x2 && (value & 0xff) == ';') {
			final_result = magazine.top();
			magazine.pop();
		}
		else if (((value >> 30) & 3) == 0x1) {
			unsigned int solved_val = names_vals[names[value & 0x3fffffff]];
			magazine.push(solved_val);
		}

	}

	unsigned int getFinalResult() {
		return final_result;
	}
};


int main() {
	Translator trans("trans_fs.txt", "trans_fy.txt");
	CreatePoliz poliz("poliz_rangs.txt", "poliz_auto.txt", "poliz_fy.txt");
	SolvePoliz solve_poliz;

	vector<string> lines;
	string str;
	ifstream input("input.txt");
	ofstream output("output.txt");
	while (!input.eof()) {
		getline(input, str);
		str.erase(remove(str.begin(), str.end(), ' '), str.end());
		lines.push_back(str);
	}
	input.close();
	for (size_t j = 0; j < lines.size(); j++) {
		for (size_t i = 0; i < lines[j].size(); i++) {
			trans.input(lines[j][i]);
		}
		trans.printLexems();
		vector<unsigned int> Lexems = trans.getLexems();
		for (size_t i = 0; i < Lexems.size(); i++) {
			if (poliz.input(Lexems[i])) i--;
		}
		poliz.printPoliz();
		vector<unsigned int> arr_poliz = poliz.getPoliz();
		for (size_t i = 0; i < arr_poliz.size(); i++) {
			solve_poliz.input(arr_poliz[i]);
		}
		output << last_solved_name << " = " << solve_poliz.getFinalResult() << endl;
		cout << last_solved_name << " = " << solve_poliz.getFinalResult() << endl << endl;
		names_vals[last_solved_name] = solve_poliz.getFinalResult();
		trans.clearAll();
		poliz.clearAll();
		solve_poliz.clearAll();
	}
	output.close();

	_getch();
	return 0;
}