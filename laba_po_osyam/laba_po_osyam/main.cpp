#include <iostream>
#include <Windows.h>
#include <stdio.h>
#include <conio.h>
#include <time.h>


HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

int main() {
	COORD coordinates;
	system("cls");
	coordinates.X = 40;
	coordinates.Y = 13;
	for (int i = 0; i < 120; i++) {
		coordinates.X = (coordinates.X + rand()%3 - 1) % 80;
		coordinates.Y = (coordinates.Y + rand()%3 - 1) % 25;
		SetConsoleCursorPosition(hConsole, coordinates);
		SetConsoleTextAttribute(hConsole, i);
		std::cout << i;
		system("cls");
	}
	_getch();
	return 0;
}