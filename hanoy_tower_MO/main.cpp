#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>

using namespace std;

//#define TESTING

int str_to_int(string val) {
	int iterations = 0;
	int sign = 1, result = 0;
	if (val[0] == '-') {
		sign = -1;
		val = val.substr(1);
	}
	while (!val.empty()) {
		if (iterations == 0 && val[0] == ' ') {
			val = val.substr(1);
			continue;
		}
		if (val[0] > '9' || val[0] < '0')
			break;
		iterations++;
		result = result * 10 + (val[0] - '0');
		val = val.substr(1);
	}
	
	if (iterations == 0)
		throw exception("No value was transformed");
	return result * sign;
}

vector<string> string_split(string val_to_split) {
	istringstream iss(val_to_split);
	vector<string> result(istream_iterator<string>{iss}, istream_iterator<string>());

	return result;
}

class Test {
	string name;
	unsigned int nparts;
	vector<int> costs;
	vector<vector<int>> sticks;
	int cost_sum = 0;
public:
	Test(string _name){
		name = _name;
	};

	void init_nparts(unsigned int _nparts) {
		nparts = _nparts;
	}

	unsigned int get_nparts() {
		return nparts;
	}

	unsigned int get_nsticks() {
		return costs.size();
	}
	
	void init_costs(int value) {
		costs.push_back(value);
		sticks.push_back(vector<int>());
	}

	void init_zero_stick() {
		if (sticks.empty()) {
			cout << "First you need to initialize sticks" << endl;
			return;
		}
		for (int i = 0; i < nparts; i++)
			sticks[0].push_back(nparts - i);
	}

	int move_ring(int ring_num, unsigned int from, unsigned int to) {
		if (from < 0 || to < 0 || from >= sticks.size() || to >= sticks.size())
			return -1; // wrong sticks numbers
		if (sticks[from].empty())
			return -2; // the from stick is empty
		if (*sticks[from].end() != ring_num)
			return -3; // on the top of the from stick is not ring_num ring
		if (!sticks[to].empty() && *sticks[from].end() >= *sticks[to].end())
			return -4; // cant place bigger ring on the smaller ring

		sticks[to].push_back(*sticks[from].end());
		cost_sum = cost_sum + costs[to];
		sticks[from].pop_back();
		return 0;
	}
};

enum FSM_enum {
	WAIT_FOR_INIT_COMMAND = 0,
	WAIT_FOR_NPARTS_COMMAND,
	WAIT_FOR_NPARTS_END,
	WAIT_FOR_COST_COMMAND,
	WAIT_FOR_COST_VALUES,
	WAIT_FOR_COST_END,
	WAIT_FOR_ORDER_COMMAND,
	WAIT_FOR_ORDER_VALUES,
	END
};

#define DEBUG

#define END_STR string("/")
#define INIT_STR string("INIT")
#define NPARTS_STR string("NPARTS")
#define COST_STR string("COST")
#define ORDER_STR string("ORDER")

#ifdef DEBUG
#define DEBUG_PRINTF printf
#else
#define DEBUG_PRINTF
#endif

struct node {
	vector<vector<int>> sticks;
};




class doing_shit {
	unsigned int nparts;
	vector<int> costs;
	vector<vector<int>> sticks;
	int cost_sum = 0;

public:
	doing_shit() {

	}

	void init_nparts(unsigned int _nparts) {
		nparts = _nparts;
	}

	unsigned int get_nparts() {
		return nparts;
	}
	
	unsigned int get_nsticks() {
		return costs.size();
	}

	void init_costs(int value) {
		costs.push_back(value);
		sticks.push_back(vector<int>());
	}

	void init_zero_stick() {
		if (sticks.empty()) {
			cout << "First you need to initialize sticks" << endl;
			return;
		}
		for (int i = 0; i < nparts; i++)
			sticks[0].push_back(nparts - i);
	}

	int move_ring(int ring_num, unsigned int from, unsigned int to) {
		if (from < 0 || to < 0 || from >= sticks.size() || to >= sticks.size())
			return -1; // wrong sticks numbers
		if (sticks[from].empty())
			return -2; // the from stick is empty
		if (*sticks[from].end() != ring_num)
			return -3; // on the top of the from stick is not ring_num ring
		if (!sticks[to].empty() && *sticks[from].end() >= *sticks[to].end())
			return -4; // cant place bigger ring on the smaller ring

		sticks[to].push_back(*sticks[from].end());
		cost_sum = cost_sum + costs[to];
		sticks[from].pop_back();
		return 0;
	}

	int findPath(unsigned int stick_to) {
		int result_cost = 0;

		/*  */

		return result_cost;
	}
};






int main() {
#ifdef TESTING
	vector<Test> tests_array;
	FSM_enum FSM_position = WAIT_FOR_INIT_COMMAND;
	ifstream input_file;
	input_file.open("input.txt", ios::in);

	string prevline = "default", line;
	while (!input_file.eof()) {
		getline(input_file, line);
		DEBUG_PRINTF("%s\n", line.c_str());
		switch (FSM_position)
		{
		case WAIT_FOR_INIT_COMMAND:
			DEBUG_PRINTF("WAIT_FOR_INIT_COMMAND\n");
			if (line.find(INIT_STR) != string::npos) {
				tests_array.push_back(Test(prevline));
				FSM_position = WAIT_FOR_NPARTS_COMMAND;
			}
			break;
		case WAIT_FOR_NPARTS_COMMAND:
			DEBUG_PRINTF("WAIT_FOR_NPARTS_COMMAND\n");
			if (line.find(NPARTS_STR) != string::npos) {
				tests_array.back().init_nparts(str_to_int(line.substr(line.find(NPARTS_STR) + NPARTS_STR.length())));
				FSM_position = WAIT_FOR_NPARTS_END;
			}
			break;
		case WAIT_FOR_NPARTS_END:
			DEBUG_PRINTF("WAIT_FOR_NPARTS_END\n");
			if (line.find(END_STR) != string::npos)
				FSM_position = WAIT_FOR_COST_COMMAND;
			break;
		case WAIT_FOR_COST_COMMAND:
			DEBUG_PRINTF("WAIT_FOR_COST_COMMAND\n");
			if (line.find(COST_STR) != string::npos)
				FSM_position = WAIT_FOR_COST_VALUES;
			break;
		case WAIT_FOR_COST_VALUES: {
			DEBUG_PRINTF("WAIT_FOR_COST_VALUES\n");
			vector<string> values = string_split(line);
			try {
				for (int i = 0; i < values.size(); i++)
					tests_array.back().init_costs(str_to_int(values[i]));
				FSM_position = WAIT_FOR_COST_END;
			}
			catch (const std::exception& e) {
				cout << "While WAIT_FOR_COST_VALUES: " << e.what() << endl;
			}
			break;
		}
		case WAIT_FOR_COST_END:
			DEBUG_PRINTF("WAIT_FOR_COST_END\n");
			if (line.find(END_STR) != string::npos)
				FSM_position = WAIT_FOR_ORDER_COMMAND;
			break;
		case WAIT_FOR_ORDER_COMMAND:
			DEBUG_PRINTF("WAIT_FOR_ORDER_COMMAND\n");
			if (line.find(ORDER_STR) != string::npos)
				FSM_position = WAIT_FOR_ORDER_VALUES;
			break;
		case WAIT_FOR_ORDER_VALUES: {
			DEBUG_PRINTF("WAIT_FOR_ORDER_VALUES\n");
			vector<string> values = string_split(line);
			try {
				if (line.find(END_STR) != string::npos) {
					FSM_position = END;
					break;
				}
				tests_array.back().move_ring(str_to_int(values[0]), str_to_int(values[1]), str_to_int(values[2]));
			}
			catch (const std::exception& e) {
				cout << "While WAIT_FOR_ORDER_VALUES: " << e.what() << endl;
			}
			break;
		}
		case END:
			FSM_position = WAIT_FOR_INIT_COMMAND;
			break;
		default:
			break;
		}
		prevline = line;
	}
	input_file.close();
#endif


}