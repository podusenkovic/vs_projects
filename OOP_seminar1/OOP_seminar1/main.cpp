#include <iostream>
#include <string>

#include "List.h"

using namespace std;


void printMainMenu() {
	cout << "			Main menu" << endl;
	cout << " 1. Print list" << endl;
	cout << " 2. Delete the record" << endl;
	cout << " 3. Sort the list" << endl;
	cout << " 4. Exit" << endl;
	cout << " Your choice : ";
}

void printDelete() {
	cout << endl << " Choose how you'd like to delete" << endl;
	cout << " 1. Delete by number" << endl;
	cout << " 2. Delete by value" << endl;
	cout << " 3. Delete by string" << endl;
	cout << " Your choice : ";
}

void printSort() {
	cout << endl << " Choose the way to sort the list" << endl;
	cout << " 1. Sort by number" << endl;
	cout << " 2. Sort by value" << endl;
	cout << " 3. Sort by string" << endl;
	cout << " Your choice : ";
}



int main() {
	bool keepMaining = true;
	string filename;
	int amount;
	int choice, choice2;

	cout << "Would you like to create a new file or use already created? 0/1 : ";
	cin >> choice;

	List *list;
	
	if (choice) {
		cout << "Input filename : ";
		cin >> filename;

		list = new List;
		list->readFromFile(filename);
		
		list->print();
	}
	else {
		cout << "Input filename : ";
		cin >> filename;
		
		cout << "Input amount of records : ";
		cin >> amount;

		list = new List(amount);

		cout << "Now input number, number data and string data" << endl;
		for (int i = 0; i < amount; i++) {
			double ndata; string strdata;
			cin >> ndata >> strdata;
			list->setRecord(i, i + 1, ndata, strdata);
		}
		list->writeToFile(filename);
	}		

	while (keepMaining) {
		printMainMenu();
		cin >> choice;
		switch (choice) {
		case 1: list->print(); break;
		case 2: 
			printDelete();
			cin >> choice2;
			switch (choice2) {
			case 1: 
				int num;
				cout << " Enter number of record to delete : ";
				cin >> num;
				list->deleteRecord(num);
				break;
			case 2:
				double value;
				cout << " Enter records value to delete : ";
				cin >> value;
				list->deleteRecord(value);
				break;
			case 3: string data;
				cout << " Enter records string data to delete : ";
				cin >> data;
				list->deleteRecord(data);
			}
		case 3: 			
			printSort();
			cin >> choice2;
			switch (choice2) {
			case 1:
				list->sortByNum();
				break;
			case 2:
				list->sortByValue();
				break;
			case 3:
				list->sortByString();
				break;
			}
		case 4:
			keepMaining = false;
			break;
		}
	}


	system("pause");
}