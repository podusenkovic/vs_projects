#pragma once
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

struct record {
	int number;
	double numData;
	string strData;
};

class List {
	record *Records;
	int amount;

public:
	List(int amount = 0);

	void setRecord(int i, int number, double numData, string strData);
	
	int getAmount() {
		return amount;
	}

	void writeToFile(string filename);
	void readFromFile(string filename);
	void print();
	void deleteRecord(int num);
	void deleteRecord(double num);
	void deleteRecord(string num);
	void sortByNum();
	void sortByValue();
	void sortByString();

};
