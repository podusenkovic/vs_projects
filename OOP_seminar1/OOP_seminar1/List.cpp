#include "List.h"

bool isEqual(double a, double b) {
	double eps = 0.0000001;
	if (abs(a - b) < eps)
		return true;
	else
		return false;
}

int strCmp(string a, string b) {
	if (a.length() > b.length())
		return 1;
	else if (a.length() < b.length())
		return -1;
	else {
		for (int i = 0; i < a.length(); i++) {
			if (a[i] > b[i])
				return 1;
			else if (a[i] < b[i])
				return -1;
		}
	}
	return 0;
}

void swap(record &a, record &b) {
	record temp;
	
	temp.number = a.number;
	temp.numData = a.numData;
	temp.strData = a.strData;

	a.number = b.number;
	a.numData = b.numData;
	a.strData = b.strData;

	b.number = temp.number;
	b.numData = temp.numData;
	b.strData = temp.strData;
}

List::List(int amount) {
	this->amount = amount;
	Records = new record[amount];
}

void List::setRecord(int i, int number, double numData, string strData) {
	Records[i].number = number;
	Records[i].numData = numData;
	Records[i].strData = strData;
}

void List::writeToFile(string filename) {
	ofstream file(filename);
	file << amount << endl;
	for (int i = 0; i < amount; i++)
		file << Records[i].number << " " << Records[i].numData << " " << Records[i].strData << endl;
	cout << "!!!   Succesfully write data to the file   !!!" << endl;
}

void List::readFromFile(string filename) {
	ifstream file(filename);
	file >> amount;
	Records = new record[amount];
	for (int i = 0; i < amount; i++) {
		int num; double ndata; string strdata;
		file >> num >> ndata >> strdata;
		setRecord(i, num, ndata, strdata);
	}
	file.close();
	cout << "!!!   Succesfully read data from file   !!!" << endl;
}

void List::print() {
	cout << endl;
	cout << "      List of records" << endl;
	cout << "--------------------------------------------------" << endl;
	cout << "|  #  |   Value   |              Data            |" << endl;
	cout << "--------------------------------------------------" << endl;
	for (int i = 0; i < amount; i++) {
		cout << "|";
		cout.width(3);
		cout << Records[i].number << "  |";
		cout.width(9);
		cout.precision(3);
		cout << Records[i].numData << "  |";
		cout.width(30);
		cout << Records[i].strData << "|";
		cout << endl;
	}
	cout << "--------------------------------------------------" << endl << endl;
}

void List::deleteRecord(int num) {
	int indexToDel = -1;
	for (int i = 0; i < amount; i++)
		if (Records[i].number == num)
			indexToDel = i;

	if (indexToDel == -1) {
		cout << "There aren't any records with this number" << endl;
		return;
	}

	record *newRec = new record[amount - 1];

	for (int i = 0; i < indexToDel; i++)
		newRec[i] = Records[i];
	for (int i = indexToDel + 1; i < amount; i++) {
		newRec[i - 1] = Records[i];
		//			newRec[i - 1].number--;
	}

	delete[]Records;
	Records = newRec;
	amount--;
}

void List::deleteRecord(double num) {
	int k = 0, del = 0;
	for (int i = 0; i < amount; i++) {
		if (isEqual(num, Records[i].numData))
			k++;
	}

	if (k == 0) {
		cout << "There aren't any records with this value" << endl;
		return;
	}

	record *newRec = new record[amount - k];

	for (int i = 0; i < amount; i++) {
		if (!isEqual(num, Records[i].numData)) {
			newRec[i - del] = Records[i];
		}
		else {
			del++;
			continue;
		}
	}

	delete[]Records;
	Records = newRec;
	amount -= k;
}


void List::deleteRecord(string num) {
	int k = 0, del = 0;
	for (int i = 0; i < amount; i++) {
		if (num == Records[i].strData)
			k++;
	}

	if (k == 0) {
		cout << "There aren't any records with this value" << endl;
		return;
	}

	record *newRec = new record[amount - k];

	for (int i = 0; i < amount; i++) {
		if (num != Records[i].strData) {
			newRec[i - del] = Records[i];
		}
		else {
			del++;
			continue;
		}
	}

	delete[]Records;
	Records = newRec;
	amount -= k;
}

void List::sortByNum() {
	for (int i = 0; i < amount - 1; i++) {
		for (int j = i; j < amount; j++) {
			if (Records[j].number < Records[i].number)
				swap(Records[i], Records[j]);
		}
	}
}

void List::sortByValue() {
	for (int i = 0; i < amount - 1; i++) {
		for (int j = i; j < amount; j++) {
			if (Records[j].numData < Records[i].numData)
				swap(Records[i], Records[j]);
		}
	}
}

void List::sortByString() {
	for (int i = 0; i < amount - 1; i++) {
		for (int j = i; j < amount; j++) {
			if (strCmp(Records[j].strData, Records[i].strData) == -1)
				swap(Records[i], Records[j]);
		}
	}
}


